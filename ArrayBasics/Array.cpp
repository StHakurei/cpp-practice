#include <iostream>
#include <vector>
using namespace std;

int dynamics()
{
    /* Use dynamics array. */
    vector<int> dynamicsArray (3);

    dynamicsArray[0] = 1000;
    dynamicsArray[1] = 2000;
    dynamicsArray[2] = 3000;

    cout << "Array length: " << dynamicsArray.size() << endl;

    int newElement = 4000;
    dynamicsArray.push_back(newElement);
    cout << "Array length: " << dynamicsArray.size() << endl;
    cout << "Last element: " << dynamicsArray[dynamicsArray.size() - 1] << endl;
    return 0;
}

int main()
{
    /* Define an array. */
    int dataset[5] = {0, 1, 2, 3, 4};

    /* Define an array and initial some elements and the rest will be set to 0. */
    int dataset_0[5] = {100, 200};

    /* Define an array without the number of elements if you know the all initial values. */
    int dataset_1[] = {-200, -100, 0, 100, 200};

    cout << dataset[0] << ":" << dataset[1] << ":" << dataset[2] << ":" << dataset[3] << ":" << dataset[4] << endl;
    cout << dataset_0[0] << ":" << dataset_0[1] << ":" << dataset_0[2] << ":" << dataset_0[3] << ":" << dataset_0[4] << endl;
    cout << dataset_1[0] << ":" << dataset_1[1] << ":" << dataset_1[2] << ":" << dataset_1[3] << ":" << dataset_1[4] << endl;

    /* Multidimensional array. */
    const int LENGHT = 3;
    int datasets[LENGHT][LENGHT] = {{102, 234, 123}, {200, 100, 110}, {56, 70, 99}};
    cout << datasets[0][2] << endl;
    cout << datasets[1][2] << endl;

    return dynamics();
}