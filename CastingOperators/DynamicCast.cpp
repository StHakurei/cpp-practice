#include <iostream>
#include <string>
using namespace std;

/*
    Casting is a mechanism by which programmers can temporarily or permanently change
    the interpretation of an object by the compiler.

    In general, a well-written C++ application should be no need for casting and for
    casting operators.
*/
class Account
{
protected:
    double* balance;

    virtual void Deposit(double amount) = 0;
    virtual void Withdraw(double amount) = 0;
    virtual void Checkout()
    {
        cout << "Raw balance value: " << balance << endl;
    }

    virtual ~Account() {}
};

class PersonalAccount: public Account
{
private:
    string* owner;

public:
    PersonalAccount(string owner)
    {
        balance = new double;
        this->owner = new string;
        *this->owner = owner;
    }

    void Deposit(double amount)
    {
        *balance += amount;
    }

    void Withdraw(double amount)
    {
        if (amount <= *balance)
        {
            *balance -= amount;
        }
        else
        {
            cout << "Insufficient balance." << endl;
        }
    }

    void Checkout()
    {
        cout << "Account Type: Personal Account." << endl;
        cout << "Account Owner: " << *owner << endl;
        cout << "Balance $" << *balance << endl;
    }

    void GetInterest()
    {
        cout << "Saving interest on qualifying account per year: $" << *balance * 0.02 << endl;
    }

    ~PersonalAccount()
    {
        if (balance != NULL)
            delete balance;

        if (owner != NULL)
            delete owner;
    }
};

class BusinessAccount: public Account
{
private:
    string* entity;
    string* taxId;

public:
    BusinessAccount(string entity, string taxId)
    {
        balance = new double;
        this->entity = new string;
        this->taxId = new string;
        *this->entity = entity;
        *this->taxId = taxId;
    }

    void Deposit(double amount)
    {
        *balance += amount;
    }

    void Withdraw(double amount)
    {
        if (amount <= *balance)
        {
            *balance -= amount;
        }
        else
        {
            cout << "Insufficient balance." << endl;
        }
    }

    void Checkout()
    {
        cout << "Account Type: Business Account." << endl;
        cout << "Account Owner: " << *entity << endl;
        cout << "Federal Tax Id: " << *taxId << endl;
        cout << "Balance $" << *balance << endl;
    }

    void CalculateTax()
    {
        cout << "Federal Tax - (ID:" << taxId << ") $" << *balance * 0.1 << endl;
    }

    ~BusinessAccount()
    {
        if (balance != NULL)
            delete balance;

        if (entity != NULL)
            delete entity;
        
        if (taxId != NULL)
            delete taxId;
    }
};

// Dynamic cast.
void DetectAccountType(Account* object)
{
    PersonalAccount* pObject = dynamic_cast <PersonalAccount*>(object);
    if (pObject) // Check success of cast.
    {
        cout << "Detected Personal Account. Get Saving interest:" << endl;
        pObject->GetInterest();
    }

    BusinessAccount* bObject = dynamic_cast <BusinessAccount*>(object);
    if (bObject) // Check success of cast.
    {
        cout << "Detected Business Account. Calculate Federal Tax:" << endl;
        bObject->CalculateTax();
    }
}

int main()
{
    PersonalAccount hakurei("Hakurei Reimu");
    BusinessAccount google("Google Inc.", "770493581");
    hakurei.Deposit(100000.00);
    google.Deposit(100000.00);

    DetectAccountType(&hakurei);
    DetectAccountType(&google);
    return 0;
}