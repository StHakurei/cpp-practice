#include <iostream>
#include <string.h>
using namespace std;

class Account
{
public:
    string name;
    string address;
    double balance = 0.00;

    void CheckStatus()
    {
        cout << "Account Info" << endl;
        cout << "Owner: " << name << endl;
        cout << "Billing address: " << address << endl;
        cout << "Balance: $" << balance << endl;
    }
};

int main()
{
    // Accessing members using the Dot operator.
    Account peter;
    peter.name = "Peter";
    peter.address = "Somewhere on the Earth.";
    peter.balance = 100.00;
    peter.CheckStatus();

    // Accessing members using the pointer operator.
    Account* hakurei = new Account();
    hakurei->name = "Hakurei";
    hakurei->address = "Somewhere on the Moon.";
    hakurei->balance = 97.25;
    hakurei->CheckStatus();
    delete hakurei;
    return 0;
}