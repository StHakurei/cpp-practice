#include <iostream>
#include <string.h>
using namespace std;

/*
    Operator sizeof() is also valid for classes, and it basically report the sum of bytes consumed
    by each data attribute within the class declaration.
*/
class Human
{
private:
    string* name;
    int* age;

public:
    Human(string name, int age)
    {
        this->name = new string;
        this->age = new int;
        *this->name = name;
        *this->age = age;
    }

    Human(const Human& source)
    {
        this->name = new string;
        this->age = new int;
        *this->name = *source.name;
        *this->age = *source.age;
    }

    ~Human()
    {
        delete name;
        delete age;
    }

    void ShowAddress()
    {
        cout << "Name: " << *name << " - " << name << endl;
        cout << "Age: " << *age << " - " << age << endl;
    }

    void ShowSize()
    {
        cout << "Name size - " << sizeof(name) << endl;
        cout << "Age size - " << sizeof(age) << endl;
    }
};

int main()
{
    Human firstMan("Peter", 100);
    firstMan.ShowAddress();
    // Deepcopy & clone through constructor.
    Human secondMan(firstMan);
    secondMan.ShowAddress();

    cout << "sizeof(firstMan)" << sizeof(firstMan) << endl;
    cout << "firstMan's attributes size:" << endl;
    firstMan.ShowSize();
    cout << "sizeof(secondMan)" << sizeof(secondMan) << endl;
    cout << "secondMan's attributes size:" << endl;
    secondMan.ShowSize();    
    return 0;
}
