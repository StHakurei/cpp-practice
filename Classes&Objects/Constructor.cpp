#include <iostream>
#include <string.h>
using namespace std;

/*
    Ths similar concept in Java may be called "JavaBean",
    which is a class that encapsulate many objects into a single object,
    and has a non-arguments constructor, it allows access of properties by using
    setter or getter functions.
*/
class Account
{
private:
    string name;
    string address;
    double balance;

public:
    Account()
    {
        this->balance = 0.00;
    }

    void IssueAccount(string name, string address)
    {
        this->name = name;
        this->address = address;
    }

    void Deposit(double amount)
    {
        this->balance = this->balance + amount;
    }

    void Withdraw(double amount)
    {
        this->balance = this->balance - amount;
    }

    void CheckStatus()
    {
        cout << "Account Info" << endl;
        cout << "Owner: " << this->name << endl;
        cout << "Billing address: " << this->address << endl;
        cout << "Balance: $" << this->balance << endl;
    }
};

// Overloading constructor.
class BusinessAccout
{
private:
    string name;
    string address;
    double balance;

public:
    BusinessAccout()
    {
        name = "N/A";
        address = "N/A";
        balance = 0.00;
        cout << "Empty business account has been created." << endl; 
    }

    BusinessAccout(string name, string address)
    {
        this->name = name;
        this->address = address;
        this->balance = 0.00;
    }

    void Registration(string name, string address)
    {
        this->name = name;
        this->address = address;
    }

    void CheckStatus()
    {
        cout << "Account Info" << endl;
        cout << "Orgnization: " << this->name << endl;
        cout << "Billing address: " << this->address << endl;
        cout << "Balance: $" << this->balance << endl;
    }
};

// Constructor parameters with default values.
class GovAccount
{
private:
    string name;
    string department;
    long double balance;

public:
    GovAccount(string name = "Federal government of the United States",
               string department = "President's Cabinet",
               long double amount = 0.00)
    {
        this->name = name;
        this->department = department;
        this->balance = amount;
    }

    void CheckStatus()
    {
        cout << "Account Info" << endl;
        cout << "Gov.Name: " << this->name << endl;
        cout << "Department: " << this->department << endl;
        cout << "Balance: $" << this->balance << endl;
    }
};

int main()
{
    Account* andrew = new Account();
    andrew->IssueAccount("Andrew", "St.Andrew Town, Scotland, UK");
    andrew->CheckStatus();
    andrew->Deposit(2000);
    andrew->Withdraw(125.99);
    andrew->CheckStatus();
    delete andrew;

    // Use overloading constructors.
    BusinessAccout* hp = new BusinessAccout();
    hp->Registration("HP", "Palo Alto City, CA, USA");
    hp->CheckStatus();

    BusinessAccout* ibm = new BusinessAccout("IBM", "Armonk Town, NY, USA");
    ibm->CheckStatus();

    // Default values.
    GovAccount* federal = new GovAccount();
    federal->CheckStatus();

    GovAccount* ca = new GovAccount("Politics and Government of California",
                                    "Executive",
                                    2000.00);
    ca->CheckStatus();

    delete hp, ibm;
    delete federal, ca;
    return 0;
}