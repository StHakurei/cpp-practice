#include <iostream>
using namespace std;

/*
    Keyword explicit will block unintentional implicit conversions.
    The reason you want to do this is to avoid accidental construction that can hide bugs.
*/
class Note
{
private:
    int num;

public:
    Note(int number): num(number)
    {
        num = number;
    }

    int GetContent()
    {
        return num;
    }
};

void PrintNote(Note object)
{
    cout << object.GetContent() << endl;
}

int main()
{
    Note myNote(1000);
    Note otherNote = Note(256);
    PrintNote(myNote);
    PrintNote(otherNote);
    /*
    Without explicit keyword, the following code will be executed without error.

    PrintNote(123); This line of code will print number '123' and convert '123' to Note.
    Note wrongNote = 5123; This constructor will accept 5123 and convert int to Note.
    */
    return 0;
}