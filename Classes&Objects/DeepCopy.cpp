#include <iostream>
#include <string.h>
using namespace std;

class Note
{
private:
    string* pointerOfNote = new string;

public:
    Note(string input)
    {
        *pointerOfNote = input;
    }

    void Check()
    {
        cout << *pointerOfNote << " stored at " << pointerOfNote << endl;
    }

    Note deepCopy()
    {
        return Note(*pointerOfNote);
    }
};

int main()
{
    /*
        Shallow copy.
        You can see that two string are pointed to same memory location.
    */
    Note object0("Welcome");
    object0.Check();

    Note object1 = object0;
    object1.Check();

    /*
        Deep copy.
        You can see that two strings are stored at different memory location.
    */
    Note object2("Good");
    object2.Check();

    Note object3 = object2.deepCopy();
    object3.Check();
    return 0;
}