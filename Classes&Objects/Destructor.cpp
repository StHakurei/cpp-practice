#include <iostream>
#include <string.h>
using namespace std;

/*
    Destructor is a special function, it's always invoked when an object of a class
    being destroyed. You could use destructor to release dynamically allocated memory
    and other resources.
*/
class SecretNote
{
private:
    int size = 0;
    string* pointerToSecret = NULL;

public:
    SecretNote(string input[], int size)
    {
        this->size = size;
        cout << "Calling object, building up..." << endl;
        pointerToSecret = new string[size];

        for (int i = 0; i < size; i++)
        {
            *pointerToSecret = input[i];
            if (i < size - 1)
            {
                ++pointerToSecret;
            }
        }
        
        // Restore pointer to the first position.
        for (int i = 0; i < size -1; i++)
        {
            --pointerToSecret;
        }
    }

    void Check()
    {
        for (int i = 0; i < size; i++)
        {
            cout << *pointerToSecret << " stored at " << pointerToSecret << endl;
             if (i < size - 1)
            {
                ++pointerToSecret;
            }
        }
        
        // Restore pointer to the first position.
        for (int i = 0; i < size -1; i++)
        {
            --pointerToSecret;
        }
    }

    ~SecretNote()
    {
        cout << "Involing destructor, cleaning up..." << endl;
        delete[] pointerToSecret;
    }
};

int main()
{
    string myNote[3] = {"Mars", "Reception", "Terminology"};
    SecretNote object(myNote, 3);
    object.Check();
    return 0;
}