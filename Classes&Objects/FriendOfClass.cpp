#include <iostream>
#include <string.h>
using namespace std;

/*
    Keyword friend grant external access of private members.
*/
class Human
{
private:
    string name;
    int age;
    friend void Talk(const Human& object);

public:
    Human(string name, int age)
    {
        this->name = name;
        this->age = age;
    }
};

void Talk(const Human& object)
{
    cout << "My name is " << object.name << ", I'm " << object.age << " years old." << endl;
}

int main()
{
    Human peter("Peter", 45);
    Talk(peter);
    return 0;
}
