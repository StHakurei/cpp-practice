#include <iostream>
#include <string.h>
using namespace std;

/*
    Singleton class that permits a single instance.
*/
class Account
{
private:
    string* name = NULL;
    double* balance = NULL;

    Account()
    {
        cout << "Private constructor was calling." << endl;
        name = new string;
        balance = new double;
    }

    ~Account()
    {
        cout << "Private destructor was calling." << endl;
        delete name;
        delete balance;
    }

public:
    static Account* GetInstance()
    {
        static Account* object = new Account();
        return object;
    }

    static void DestroyInstance(Account* instance)
    {
        delete instance;
    }

    void SetName(string name)
    {
        *this->name = name;
        *balance = 0.00;
    }

    string GetName()
    {
        return *name;
    }

    double GetBalance()
    {
        return *balance;
    }
};

int main()
{
    Account* peter = Account::GetInstance();
    peter->SetName("Peter");
    cout << "Account owner " << peter->GetName() << " with balance: $" << peter->GetBalance() << endl;
    cout << "Pointer of object: " << peter << endl;
    /*
        delete peter;

        Above code will return an error message, bcause private destructor cannot be invoked.

        SingletonClass.cpp: In function ‘int main()’:
        SingletonClass.cpp:64:12: error: ‘Account::~Account()’ is private within this context
           64 |     delete peter;
              |            ^~~~~
        SingletonClass.cpp:22:5: note: declared private here
           22 |     ~Account()
              |     ^
    */
    // Use static member to release memory.
    Account::DestroyInstance(peter);
    return 0;
}