#include <iostream>
#include <string.h>
using namespace std;

/*
    A Keyword struct is similar to a class.
    Members in a struct are public by default, unless specified.
*/
struct Human
{
    // public by default.
    string* name;
    int* age;

    Human()
    {
        name = new string;
        age = new int;
    }

    ~Human()
    {
        delete name;
        delete age;
    }

    void Talk()
    {
        cout << "My name is " << *name << ", I'm " << *age << " years old." << endl;
    }
};

int main()
{
    Human firstMan, secondMan;
    *firstMan.name = "Hakurei";
    *firstMan.age = 17;
    firstMan.Talk();

    *secondMan.name = "Galahad";
    *secondMan.age = 27;
    secondMan.Talk();
    return 0;
}
