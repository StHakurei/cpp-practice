#include <iostream>
#include <string.h>
using namespace std;

// Union often is used as member of a structure to model a complex data tyep.
union SimpleUnion
{
    int number;
    char alphabet;
};

struct ComplexType
{
    enum DataType
    {
        Int,
        Char
    } Type;

    union DataValue
    {
        int num;
        char alphabet;

        DataValue() {}
        ~DataValue() {}
    } Value;
};

void DisplayComplexData(const ComplexType& object)
{
    switch (object.Type)
    {
        case ComplexType::Int:
            cout << "ComplexType contains: " << object.Value.num << endl;
            break;
        case ComplexType::Char:
            cout << "ComplexType contains: " << object.Value.alphabet << endl;
            break;
    }
}

int main()
{
    SimpleUnion u1, u2;
    u1.number = 100;
    u2.alphabet = 'C';
    cout << "Union 1 contains an interger: " << " sizeof(u1) = " << sizeof(u1) << endl;
    cout << "Union 2 contains a character: " << " sizeof(u2) = " << sizeof(u2) << endl;

    ComplexType t1, t2;
    t1.Type = ComplexType::Int;
    t1.Value.num = 200;
    t2.Type = ComplexType::Char;
    t2.Value.alphabet = 'A';

    DisplayComplexData(t1);
    DisplayComplexData(t2);
    return 0;
}