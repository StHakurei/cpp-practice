#include <iostream>
#include <string>
#include <string.h>
#include <limits>
using namespace std;

void switchcase()
{
    enum ServantClass
    {
        Saber = 0,
        Lancer,
        Archer,
        Caster,
        Assassin,
        Rider,
        Berserker
    };

    cout << "Find the servant class affinity." << endl;
    cout << "Enter the id of servant class (Saber = 0):";

    int classCode = 0;
    cin >> classCode;
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    switch(classCode)
    {
        case Saber:
            cout << "Saber is effective to Lancer and resist to Archer." << endl;
            break;
        case Lancer:
            cout << "Lancer is effective to Archer and resist to Saber." << endl;
            break;
        case Archer:
            cout << "Archer is effective to Saber and resist to Lancer." << endl;
            break;
        case Caster:
            cout << "Caster is effective to Assassin and resist to Rider." << endl;
            break;
        case Assassin:
            cout << "Assassin is effective to Rider and resist to Caster." << endl;
            break;
        case Rider:
            cout << "Rider is effective to Caster and resist to Assassin." << endl;
            break;
        case Berserker:
            cout << "Berserker is effective to others and resist to others." << endl;
            break;
        default:
            cout << "Extra classes such as Ruler, Avenger are not includes in this version." << endl;
            break;
    }
}

void conditionOps()
{
    cout << "Condition Operator." << endl;
    cout << "Enter two numbers:";
    int numA = 0, numB = 0;
    cin >> numA;
    cin >> numB;
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    int max = (numA > numB)? numA : numB;
    cout << "The greater number is " << max << endl;
}

int main()
{
    /* if - else. */
    cout << "Enter a number:";
    int num = 0;
    cin >> num;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    /* 
        The best practice of using cin is NOT MIX the different types of input methods.
        The cin doesn't remove the newline character from the stream or do type-checking,
        therefore anyone using "cin >> integer;" and follow it up with another "cin >> string-type;"
        or "getline()" wii receive empty inputs.
    */

    if (num < 100)
    {
        cout << "The number you gave is smaller than 100." << endl;
    }
    else
    {
        cout << "The number you gave is greater than 100." << endl;
    }
    
    cout << "Enter a sentence:";
    string userInput = "";
    getline(cin, userInput);

    if (userInput.length() > 10)
    {
        cout << "Length > 10" << endl;
    }
    else if (userInput.length() == 10)
    {
        cout << "Bingo!" << endl;
    }
    else
    {
        cout << "Length < 10" << endl;
    }

    /* switch-case */
    switchcase();
    conditionOps();

    return 0;
}