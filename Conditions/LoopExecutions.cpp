#include <iostream>
#include <string>
#include <string.h>
using namespace std;

void Goto()
{
    cout << "LoopExecution: goto." << endl;
    Start:
        int a = 0, b = 0;
        cout << "Enter two numbers:";
        cin >> a;
        cin >> b;
        int max = (a > b)? a : b;
        cout << "Greater number is: " << max << endl;

        char userChoice = 'Y';
        cout << "Do you want to try again?(Y/N):";
        cin >> userChoice;

        if (userChoice == 'Y')
            goto Start;

        cout << "Goodbye!" << endl;
}

void While()
{
    cout << "LoopExecution: While" << endl;
    char userChoice = 'Y';
    while (userChoice == 'Y') {
        int a = 0, b = 0;
        cout << "Enter two numbers:";
        cin >> a;
        cin >> b;
        int max = (a > b)? a : b;
        cout << "Greater number is: " << max << endl;

        cout << "Do you want to try again?(Y/N):";
        cin >> userChoice;
    }
    cout << "Goodbye!" << endl;
}

void DoWhile()
{
    cout << "LoopExecution: DoWhile." << endl;
    char userChoice = 'Y';
    do
    {
        int a = 0, b = 0;
        cout << "Enter two numbers:";
        cin >> a;
        cin >> b;
        int max = (a > b)? a : b;
        cout << "Greater number is: " << max << endl;

        cout << "Do you want to try again?(Y/N):";
        cin >> userChoice;
    } while (userChoice == 'Y');
    cout << "Goodbye!" << endl;
}

void ForLoop()
{
    cout << "LoopExecution: for." << endl;
    const int ARRAY_LENGTH = 10;
    int sampleArray[ARRAY_LENGTH] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
    for (int i = 0; i < ARRAY_LENGTH; i++)
    {
        cout << "Index " << i << " = " << sampleArray[i] << endl;
    }

    cout << "LoopExecution: for (Range Based)." << endl;
    string nameList[6] = {"Peter", "Medusa", "Galahad", "Andrew", "August", "Gilgamesh"};
    for (string name : nameList)
    {
        /* Call continue will skip the following block of code. */
        if (name == "Galahad")
        {
            cout << "Call continue." << endl; 
            continue;
        }

        cout << name << endl;
        
        /* Call break will end this loop execution. */
        if (name == "August")
        {
            cout << "Call break." << endl;
            break;
        }
    }
}

int main()
{
    Goto();
    While();
    DoWhile();
    ForLoop();
    return 0;
}