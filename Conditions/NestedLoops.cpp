#include <iostream>
#include <string>
#include <string.h>
using namespace std;

void Fibonacci()
{
    const int MAX_NUMBER = 100;
    int a = 0;
    int b = 1;
    int c = 0;
    cout << a << endl;
    cout << b << endl;
    do
    {
        c = a + b;
        cout << c << endl;
        int temp = b;
        a = b;
        b = c;

    } while (c < MAX_NUMBER);
}

int main()
{
    const int ARRAY_LENGTH = 3;
    string addressBook[ARRAY_LENGTH][ARRAY_LENGTH] = {{"Arthur", "Male", "England"}, 
                                {"Gilgamesh", "Male", "Mesopotamia"},
                                {"Tamamo-no-Mae", "Female", "Japan"}};
    for (int i = 0; i < ARRAY_LENGTH; i++)
    {
        for (string record : addressBook[i])
        {
            cout << record << " ";
        }
        cout << endl;
    }

    /* Fibonacci approache. */
    Fibonacci();
    return 0;
}