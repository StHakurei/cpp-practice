#include <iostream>
using namespace std;

/* Define your enumeration. */
enum AirbusModel
{
    ShortFlight = 319,
    Domestic = 320,
    International = 330,
    Super = 380
};

enum Colors
{
    Red,
    White,
    Blue
};

enum WindSpeed
{
    North = 100,
    East,
    South,
    West
};

int main()
{
    /* Simple usage. */
    cout << "Airbus airplane model recommendation: " << endl;
    cout << "For short domestic flight: A" << ShortFlight << endl;
    cout << "For most commen domestic flight: A" << Domestic << endl;
    cout << "For international or long range domestic flight: A" << International << endl;
    cout << "For heavy international exchange flight: A" << Super << endl;
    
    /* User-defined types. See the output value of uninitialze Red. */
    Colors myFavorite = Red;
    cout << "My favorite color: " << myFavorite << endl;

    /* Each value in enum will increase by 1. */
    cout << North << ":" << East << ":" << South << ":" << West << endl;
}