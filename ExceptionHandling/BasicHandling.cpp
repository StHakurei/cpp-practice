#include <iostream>
#include <exception>
using namespace std;

int main()
{
    cout << "Enter the length of array: ";
    int length = 0;
    cin >> length;

    try
    {
        int* array = new int[length];
        delete[] array;
    }
    catch (bad_alloc& error)
    {
        cerr << "Exception encounterred: " << error.what() << endl;
    }
    catch (...) // Default exception.
    {
        cerr << "Unknown error occurred!" << endl;
    }
    return 0;
}