#include <exception>
#include <iostream>
#include <string>
using namespace std;

class MyException: public exception
{
private:
    string rootcause;

public:
    MyException(const char* why): rootcause(why) {}

    // Re-define virtual function to return the error message.
    virtual const char* what() const throw()
    {
        return rootcause.c_str();
    }
};

void CreateArray(int length)
{
    if (length == 0)
    {
        throw MyException("MyException: 0 length of array is prohibited in this program.");
    }

    if (length < 0)
    {
        throw MyException("MyException: negative length of an array is illegal.");
    }

    int* array = new int[length];
    delete[] array;

    cout << "Successfully tested array creation!" << endl;
}

int main()
{
    cout << "Enter length of array:";
    int input = 0;
    cin >> input;
    try
    {
        CreateArray(input);
    }
    catch (exception& error)
    {
        cerr << error.what() << endl;
    }
    catch (...)
    {
        cerr << "Unknown error has occurred." << endl;
    }
    return 0;
}