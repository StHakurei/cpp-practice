#include <iostream>
using namespace std;

double Divide(double a, double b)
{
    if (b == 0){
        throw "divisor cannot be set to 0!";
    }

    return (a / b);
}

int main()
{
    try
    {
        cout << "10 / 5 = " << Divide(10, 5) << endl;
        cout << "10 / 0 = " << Divide(10, 0) << endl;
    }
    catch (const char* error)
    {
        cerr << "Exception: " << error << endl;
    }
    return 0;
}