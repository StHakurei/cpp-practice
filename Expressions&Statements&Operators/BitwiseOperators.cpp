#include <iostream>
#include <bitset>
using namespace std;

int main()
{
    cout << "Give a number (0 - 255): ";
    unsigned short inputNum = 0;
    cin >> inputNum;
    
    /* Convert decimal number into binary. */
    bitset<8> inputBits (inputNum);
    cout << "Binary: " << inputBits << endl;

    /* Bitwise NOT.
       1 = 0
       0 = 1
    */
    bitset<8> sampleTarget (200);
    bitset<8> result0 = (~inputBits);
    cout << "Bitwise " << inputBits << " NOT = " << result0 << endl;

    /* Bitwise AND.
       1 & 1 = 1
       1 & 0 = 0
       0 & 0 = 0
    */
    bitset<8> result1 = (sampleTarget & inputBits);
    cout << "Bitwise " << sampleTarget << " AND " << inputBits << " = " << result1 << endl;

    /* Bitwise OR.
       1 | 0 = 1
       1 | 1 = 1
       0 | 0 = 0
     */
    bitset<8> result2 = (sampleTarget | inputBits);
    cout << "Bitwise " << sampleTarget << " | " << inputBits << " = " << result2 << endl;

    /* Bitwise XOR.
       1 ^ 1 = 0
       1 ^ 0 = 1
       0 ^ 0 = 0
    */
    bitset<8> result3 = (sampleTarget ^ inputBits);
    cout << "Bitwise " << sampleTarget << " ^ " << inputBits << " = " << result3 << endl;
    return 0;
}