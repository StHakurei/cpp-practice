#include <iostream>
using namespace std;

int main()
{
    /* See what's the difference. */
    cout << "Truth will set you free." << endl;
    cout << "In thy light \
             I can see the light" << endl;
    cout << "The heavens declare the glory of God; "
            "and the firmament sheweth his handywork." << endl;
    return 0;
}