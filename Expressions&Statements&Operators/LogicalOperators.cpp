#include <iostream>
using namespace std;

int main()
{
    bool a = 1; // true
    bool b = 0; // false

    cout << "a AND b = " << (a && b) << endl;
    cout << "a OR b = " << (a || b) << endl;
    return 0;
}