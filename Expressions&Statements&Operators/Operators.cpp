#include <iostream>
using namespace std;

int main()
{
    cout << "Give a number: " << endl;
    int num = 0;
    cin >> num;

    int sum = num + num;
    int product = num * num;
    int quotient = num / 2;
    int difference = num - quotient;

    cout << "Sum: " << sum << endl;
    cout << "Product: " << product << endl;
    cout << "Quotient: " << quotient << endl;
    cout << "Difference: " << difference << endl;

    /* Prefix and Postfix. */
    int origin = num;
    int postfixNum = origin++;
    cout << num << " = Value++ (Postfix): " << postfixNum << endl;
    
    origin = num;
    int prefix = ++origin;
    cout << num << " = ++Value (Prefix): " << prefix << endl;
    cout << "Therefore, if you mean a = b + 1, use prefix, a = ++b." << endl;

    /* Relational Operators. */
    int max = 100;
    bool a = (max = 100); // true  1
    bool b = (max < 200); // true  1
    bool c = (max > 200); // false 0
    cout << "A:" << a <<
            "B:" << b <<
            "C:" << c << endl;
    return 0;
}