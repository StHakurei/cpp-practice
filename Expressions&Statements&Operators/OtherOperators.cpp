#include <iostream>
#include <string.h>
using namespace std;

int memorySize()
{
    int integer = 2500;
    int intArray [100] = {0};
    string words ("See the sunset.");
    
    /* Get occupied memory size. */
    cout << "integer occupied memory: " << sizeof(integer) << endl;
    cout << "intArray occupied memory: " << sizeof(intArray) << endl;
    cout << "string occupied memory: " << sizeof(words) << endl;
    return 0;
}

int main()
{
    /* Compound Assignment Operators. */
    int a = 20000000;
    int b = 10000000;

    cout << "a = 10" << endl;
    cout << "b = 20" << endl;
    
    /* a = a + b */
    cout << "Addition (a += b): " << (a += b) << endl;

    a = 20;
    b = 10;

    /* a = a - b */
    cout << "Subtraction (a -= b): " << (a -= b) << endl;

    a = 20;
    b = 10;

    /* a = a * b */
    cout << "Multiplication (a *= b): " << (a *= b) << endl;

    a = 20;
    b = 10;

    /* a = a / b */
    cout << "Division (a /= b): " << (a / b) << endl;

    a = 20;
    b = 10;

    /* a = a % b */
    cout << "Modulo (a %= b): " << (a %= b) << endl;
    return memorySize();
}