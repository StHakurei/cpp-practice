#include <iostream>
using namespace std;

int main()
{
    int original = 128;
    int doubleNum = original << 1;
    int quadrupleNum = original << 2;
    int halfNum = original >> 1;
    int quarterNum = original >> 2;

    cout << "Original Number: " << original << endl;
    cout << "Double: " << doubleNum << endl;
    cout << "Quadruple: " << quadrupleNum << endl;
    cout << "Half: " << halfNum << endl;
    cout << "Quarter: " << quarterNum << endl;
    return 0;
}