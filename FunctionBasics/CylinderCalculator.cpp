#include <iostream>
using namespace std;

/* Define a function with default param. */
double calculate(double radius, double height, double pi = 3.14)
{
    return radius * 2 * pi * height;
}

int main()
{
    double radius = 0.00, height = 0.00;
    cout << "Enter cylinder's radius(cm):";
    cin >> radius;

    cout << "Enter cylinder's height(cm):";
    cin >> height;
    
    cout << "Customize Pi? (Enter 0 if you don't):";
    double pi;
    cin >> pi;
    
    if (pi == 0)
    {
        cout << "Cylinder capacity: " << calculate(radius, height) << endl;
    }
    else
    {
        cout << "Cylinder capacity: " << calculate(radius, height, pi) << endl;
    }
    return 0;
}