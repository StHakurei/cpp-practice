#include <iostream>
using namespace std;

/* Define an expression. */
constexpr double getPi() { return 22.0 / 7; }

/* Declare and define a function. */
int printTitle();

int printTitle()
{
    cout << "C++ Practice." << endl;
    cout << "Pi: " << getPi() << endl;
    return 0;
}

int main()
{
    /* Function call with return used to exit. */
    return printTitle();
}