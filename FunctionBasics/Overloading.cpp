#include <iostream>
#include <string.h>
#include <string>
using namespace std;

void findAffinity(string className)
{
    if (className == "Saber")
    {
        cout << "Saber is effective to Lancer and resist to Archer." << endl;
    }
    else if (className == "Lancer")
    {
        cout << "Lancer is effective to Archer and resist to Saber." << endl;
    }
    else if (className == "Archer")
    {
        cout << "Archer is effective to Saber and resist to Lancer." << endl;
    }
    else
    {
        cout << "Unknown class." << endl;
    }
}

void findAffinity(int classCode)
{
    switch(classCode)
    {
        case 0:
            cout << "Saber is effective to Lancer and resist to Archer." << endl;
            break;
        case 1:
            cout << "Lancer is effective to Archer and resist to Saber." << endl;
            break;
        case 2:
            cout << "Archer is effective to Saber and resist to Lancer." << endl;
            break;
        default:
            cout << "Unknown class." << endl;
            break;
    }
}

int main()
{
    cout << "Find servant affinity by class name(Saber, Lancer, Archer):";
    string theName = "";
    getline(cin, theName);
    findAffinity(theName);

    cout << "Find servant affinity by class code(0-2):";
    int theCode = 0;
    cin >> theCode;
    findAffinity(theCode);
    return 0;
}