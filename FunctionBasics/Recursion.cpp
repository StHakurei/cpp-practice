#include <iostream>
using namespace std;

void Fibonacci(int a, int b, int limit)
{
    int c = a + b;
    if (c < limit)
    {
        cout << c << endl;
        Fibonacci(b, c, limit);
    }
}

int main()
{
    cout << "Fibonacci approache via recursion function." << endl;
    cout << "Give a limit:";
    int limit = 100;
    cin >> limit;
    cout << "0" << endl;
    cout << "1" << endl;
    Fibonacci(0, 1, limit);
    return 0;
}