#include <iostream>
#include <string.h>
using namespace std;

/*
    Having an instance of parent class in private member instead of inheriting from it
    is called composition or aggregation. The sample code in below.
*/

class Account
{
public:
    string owner;

    void SetOwner(string owner)
    {
        this->owner = owner;
    }

    void GetOwner()
    {
        cout << "Account owner: " << owner << endl;
    }
};

class PremiumAccount
{
private:
    Account account;

public:
    PremiumAccount(string customer)
    {
        account.SetOwner(customer);
    }

    void ShowCustomer()
    {
        account.GetOwner();
    }
};

int main()
{
    PremiumAccount premium("Okita");
    premium.ShowCustomer();
    return 0;
}