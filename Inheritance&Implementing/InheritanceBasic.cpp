#include <iostream>
#include <string.h>
using namespace std;

class BankAccount
{
protected: // accessible only to devired classes.
    string owner;
    bool enabled;

    BankAccount(string owner)
    {
        this->owner = owner;
    }

public:
    void CheckAccount()
    {
        cout << "Name: " << owner << endl;
        cout << "Status: " << enabled << endl;
    }
};

class DebitAccount: public BankAccount
{
private:
    double balance;

public:
    DebitAccount(string name): BankAccount(name) // Passing parameters to the base class.
    {
        enabled = true;
        balance = 0.00;
    }

    bool GetAccountStatus()
    {
        return enabled;
    }

    double GetBalance()
    {
        return balance;
    }

    void CloseAccount()
    {
        enabled = false;
    }

    void Deposit(double amount)
    {
        if (enabled)
        {
            balance = balance + amount;
        }
        else
        {
            cout << "Account is disabled." << endl;
        }
    }

    void Withdraw(double amount)
    {
        if (enabled)
        {
            if (amount <= balance)
            {
                balance = balance - amount;
            }
            else
            {
                cout << "Insuffecient balance." << endl;
            }
        }
        else
        {
            cout << "Account is disabled." << endl;
        }
    }

    void CheckAccount() // Overriding base classe's method.
    {
        cout << "Account owner: " << owner << endl;
        cout << "Account is enabled: " << enabled << endl;
        cout << "Balance - $" << balance << endl;
    }
};

class CreditAccount: public BankAccount
{
private:
    double credits;
    double debt;

public:
    CreditAccount(string name): BankAccount(name)
    {
        credits = 500.00;
        debt = 0.00;
        enabled = true;
    }

    void CloseAccount()
    {
        enabled = false;
    }

    void SetCredits(double credits)
    {
        this->credits = credits;
    }

    void Charge(double amount)
    {
        if (enabled)
        {
            if (debt < credits && amount < (credits - debt))
            {
                debt = debt + amount;
            }
            else
            {
                cout << "Insuffecient credits." << endl;
            }
        }
        else
        {
            cout << "Account is disabled." << endl;
        }
    }

    void CheckBilling()
    {
        cout << "Account " << owner << "(" << enabled << ")" << endl;
        cout << "Authorized credits - $" << credits << endl;
        cout << "Current debt - $" << debt << endl;
    }
};

int main()
{
    // Issue a debit account to Peter.
    cout << "Peter's account." << endl;
    DebitAccount peter("Peter");
    peter.CheckAccount();
    peter.Deposit(200.00);
    cout << "Check balance - $" << peter.GetBalance() << endl;
    peter.Withdraw(199.99);
    cout << "Check balance - $" << peter.GetBalance() << endl;
    peter.Deposit(50.00);
    peter.CloseAccount();
    peter.Deposit(3.12);
    peter.CheckAccount();
    /*
        The sample of calling base classe's method instead of using derived class's.
        Because method CheckAccount() is a public method in the BankAccount class,
        so you can call it. If it's a protected method, then you can't do that.
    */
    cout << "Invoke method of the base class in a derived class." << endl;
    peter.BankAccount::CheckAccount();

    // Issue a credit account to Hakurei.
    cout << "\nHakurei's account." << endl;
    CreditAccount hakurei("Hakurei");
    hakurei.CheckBilling();
    hakurei.Charge(87.99);
    hakurei.Charge(500.99);
    hakurei.SetCredits(600);
    hakurei.Charge(500.99);
    hakurei.CheckBilling();
    hakurei.CloseAccount();
    hakurei.Charge(87.99);
    hakurei.CheckBilling();
    return 0;
}

