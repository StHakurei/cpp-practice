#include <iostream>
#include <string.h>
using namespace std;

class TurboEngine
{
public:
    string engineManufacturer;
    string engineModel;
    int powerInKn;

    void GetEngineInfo()
    {
        cout << "TurboEngine '" << engineModel << "' developed by " << engineManufacturer << endl;
        cout << "Max throttle push " << powerInKn << " Kn." << endl;
    }
};

class AuxiliaryPowerUnit
{
public:
    string apuManufacturer;
    string apuModel;
    int powerInKw;

    void GetAPUInfo()
    {
        cout << "AuxiliaryPowerUnit '" << apuModel << "' developed by " << apuManufacturer << endl;
        cout << "Max power output " << powerInKw << " Kw." << endl;
    }
};

class Certification
{
public:
    string certOrganization;
    string certCode;
    string certDate;

    void GetCertInfo()
    {
        cout << "Certification Code - " << certCode << endl;
        cout << "Certified by " << certOrganization << " on " << certDate << endl;
    }
};

// Avoid being inherited by using keyword final, so this class could not be used as base class.
class Airplane final: private TurboEngine, private AuxiliaryPowerUnit, private Certification
{
public:
    string manufacturer;
    string model;

    Airplane()
    {
        manufacturer = "Airbus";
        model = "A350 WXB";

        engineManufacturer = "Rolls-Royce";
        engineModel = "Trent XWB";
        powerInKn = 431;

        apuManufacturer = "Honeywell";
        apuModel = "HGT1700";
        powerInKw = 1300;

        certOrganization = "European Aviation Safety Agency";
        certCode = "EASA";
        certDate = "30th September 2014";
    }

    void ShowAirplane()
    {
        cout << "------ " << manufacturer << " " << model << " ------" << endl;
        GetEngineInfo();
        GetAPUInfo();
        GetCertInfo();
    }
};

int main()
{
    Airplane a350;
    a350.ShowAirplane();
    return 0;
}