#include <iostream>
#include <string.h>
using namespace std;

class Account
{
public:
    string owner;
};

class SampleAccount0: public Account
{
private:
    double balance;

public:
    SampleAccount0(string name, double balance)
    {
        owner = name;
        this->balance = balance;
    }

    void ShowAccount()
    {
        cout << "Owner: " << owner << endl;
        cout << "Balance - $" << balance << endl;
    }
};

class SampleAccount1: private Account
{
private:
    double balance;

public:
    SampleAccount1(string name, double balance)
    {
        owner = name;
        this->balance = balance;
    }

    void ShowAccount()
    {
        cout << "Owner: " << owner << endl;
        cout << "Balance - $" << balance << endl;
    }
};

int main()
{
    /*
        You can access base class's attributes in public inheritance,
        but it's impossible in private inheritance.
    */
    SampleAccount0 okita("Okita", 100.00);
    okita.ShowAccount();
    okita.owner = "Hacker";
    okita.ShowAccount();

    SampleAccount1 hakurei("Hakurei", 200.00);
    hakurei.ShowAccount();
    /*
    This line of code will trigger the compiler error.

        hakurei.owner("Hacker");

    PrivateInheritance.cpp: In function ‘int main()’:
    PrivateInheritance.cpp:62:13: error: ‘std::string Account::owner’ is inaccessible within this context
       62 |     hakurei.owner = "Hacker";
          |             ^~~~~
    PrivateInheritance.cpp:8:12: note: declared here
        8 |     string owner;
          |            ^~~~~

    */
    return 0;
}