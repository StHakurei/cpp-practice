#include <iostream>
#include <string.h>
using namespace std;

class Account
{
public:
    string owner;

    void SetOwner(string owner)
    {
        this->owner = owner;
    }

    void GetOwner()
    {
        cout << "Account owner: " << owner << endl;
    }
};

class BankAccount: protected Account
{
public:
    void SetAccount(string name)
    {
        SetOwner(name);
    }

    void GetAccount()
    {
        GetOwner();
    }
};

class CompanyAccount: protected BankAccount
{
public:
    void SetCompany(string name)
    {
        SetOwner(name);
    }

    void GetCompany()
    {
        GetOwner();
    }
};

/*
    You can see the difference of private inheritance and protected inheritance.
    In protected inheritance, grand derived class could still access methods of base class.
    But in private inheritance, grand derived class is prohibited from accessing base class's
    attributes.
*/
class PersonalAccount: private Account
{
public:
    void SetName(string name)
    {
        SetOwner(name);
    }

    void GetName()
    {
        GetOwner();
    }
};

class AgentAccount: private PersonalAccount
{
public:
    void SetUser(string name)
    {
        SetName(name);
        // SetOwner(name); <- Compiler will throw out an error for accessing of base class's method.
    }

    void GetUser()
    {
        GetName();
        // SetOwner(); <- Compiler will throw out an error for accessing of base class's method.
    }
};

int main()
{
    Account myAccount;
    myAccount.SetOwner("Hakurei");
    myAccount.GetOwner();

    BankAccount bankAccount;
    bankAccount.SetAccount("City Bank");
    bankAccount.GetAccount();

    CompanyAccount companyAccount;
    companyAccount.SetCompany("IBM");
    companyAccount.GetCompany();

    PersonalAccount person;
    person.SetName("Elizabeth");
    person.GetName();

    AgentAccount agent;
    agent.SetUser("Artoria Pendragon");
    agent.GetUser();
    return 0;
}