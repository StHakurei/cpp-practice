#include <iostream>
#include <string>
using namespace std;

/*
    Template in C++ enable you to define a behavior that you can apply to object of varying types.
*/
template <typename Type>
const void Display(const Type& value1, const Type& value2)
{
    cout << "Value 1 = " << value1 << endl;
    cout << "Value 2 = " << value2 << endl;
}

template <typename Type1, typename Type2>
const Type1& Get(const Type1& value1, const Type2& value2)
{
    cout << "Value 1 = " << value1 << endl;
    cout << "Value 2 = " << value2 << endl;
    return value1;
}

int main()
{
    string str1 = "Hi", str2 = "I'm Hakurei";
    int int1 = 1989, int2 = 0604;
    Display(str1, str2);
    Display(int1, int2);
    cout << "We get: " << Get(str1, int1) << endl;
    cout << "We get: " << Get(int2, str2) << endl;
    return 0;
}