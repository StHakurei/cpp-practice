#include <iostream>
#include <string>
using namespace std;

/*
    Use Macro #define to declare constants. the preprocessor (what runs before the compiler starts)
    will replace every instance of the marco with value they define.
*/
#define PI 3.1415926
#define RADIUS_TYPE double
#define SQUARE(x) ((x) * (x))
#define AREA_CIRCLE(r) (PI * (r) * (r))
#define FAV_WHISKY "Jeck Daniels"

int main()
{
    RADIUS_TYPE radius = 0;
    cout << "My favorite drink is " << FAV_WHISKY << endl;
    cout << "Enter radius: ";
    cin >> radius;
    cout << "Square: " << SQUARE(radius) << endl;
    cout << "Area of circle: " << AREA_CIRCLE(radius) << endl;
    return 0;
}