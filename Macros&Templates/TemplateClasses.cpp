#include <iostream>
#include <string>
using namespace std;

// Declare a template.
template <typename T1, typename T2>
class Storage
{
private:
    T1 value1;
    T2 value2;

public:
    Storage(const T1& val1, const T2& val2)
    {
        value1 = val1;
        value2 = val2;
    }

    T1& GetFirst()
    {
        return value1;
    }

    T2& GetSecond()
    {
        return value2;
    }
};

// Template with default params.
template <typename T1 = string, typename T2 = double>
class SimpleStorage
{
private:
    T1 value1;
    T2 value2;

public:
    SimpleStorage(const T1& val1, const T2& val2)
    {
        value1 = val1;
        value2 = val2;
    }

    T1& GetFirst()
    {
        return value1;
    }

    T2& GetSecond()
    {
        return value2;
    }
};

// Specialize "SimpleStorage" for types int & int here.
template <> class SimpleStorage <int, int>
{
private:
    int value1;
    int value2;

public:
    SimpleStorage(const int& val1, const int& val2)
    {
        value1 = val1;
        value2 = val2;
    }

    const int & GetFirst() const
    {
        return value1;
    }

    const int GetSecond() const
    {
        return value2;
    }

    const int GetMax() const
    {
        if (value1 < value2)
        {
            return value2;
        }
        else
        {
            return value1;
        }
    }
};

// Template instantiation with data type string and int.
int main()
{
    Storage <string, int> myStorage ("Constantinople", 330);
    cout << "Value 1 = " << myStorage.GetFirst() << endl;
    cout << "Value 2 = " << myStorage.GetSecond() << endl;

    SimpleStorage <int, int> storage (100, 50);
    cout << "Value 1 = " << storage.GetFirst() << endl;
    cout << "Value 2 = " << storage.GetSecond() << endl;
    cout << "The max = " << storage.GetMax() << endl;
    return 0;
}