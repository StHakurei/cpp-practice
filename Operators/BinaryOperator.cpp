#include <iostream>
#include <sstream>
#include <string>
using namespace std;

class StorageService
{
private:
    int day, week;
    string serviceInString;

public:
    StorageService()
    {
        day = 0;
        week = 0;
    }

    StorageService& operator + (int daysToAdd)
    {
        int weeks = daysToAdd / 7;
        int days = daysToAdd % 7;
        week = week + weeks;
        day = day + days;
        return *this;
    }

    void operator += (int daysToAdd)
    {
        int weeks = daysToAdd / 7;
        int days = daysToAdd % 7;
        week = week + weeks;
        day = day + days;
    }

    bool operator == (const StorageService& object)
    {
        return (this->day == object.day && this->week == object.week);
    }

    bool operator != (const StorageService& object)
    {
        return (this->day != object.day || this->week != object.week);
    }

    operator const char*()
    {
        ostringstream formattedContent;
        formattedContent << "Service has running for " << day << " day " << week << " week.";
        serviceInString = formattedContent.str();
        return serviceInString.c_str();
    }
};

int main()
{
    StorageService object;
    object = object + 10;
    cout << object << endl;

    object += 37;
    cout << object << endl;

    StorageService sample0;
    StorageService sample1;

    sample0 += 10;
    sample1 += 47;

    cout << "Is sample0 equals to object? " << (object == sample0) << endl;
    cout << "Is sample1 equals to object? " << (object == sample1) << endl;
    cout << "is sample0 not equals to object? " << (object != sample0) << endl;
    cout << "is sample1 not equals to object? " << (object != sample1) << endl;
    return 0;
}
