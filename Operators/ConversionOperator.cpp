#include <iostream>
#include <sstream>
#include <string>
using namespace std;

class StorageService
{
private:
    int day, week;
    string serviceInString;

public:
    StorageService()
    {
        day = 0;
        week = 0;
    }

    StorageService& operator ++ ()
    {
        ++ day;
        if (day == 7)
        {
            day = 0;
            ++ week;
        }
        return *this;
    }

    // Use conversion operator to support standard output.
    operator const char*()
    {
        ostringstream formattedContent;
        formattedContent << "Service has running for " << day << " day " << week << " week.";
        serviceInString = formattedContent.str();
        return serviceInString.c_str();
    }
};

int main()
{
    StorageService object;
    for (int i = 0; i < 32; i++)
    {
        ++ object;
    }
    cout << object << endl;
    return 0;
}