#include <iostream>
#include <string>
using namespace std;

class Car
{
private:
    string brand;

public:
    Car(string brand)
    {
        this->brand = brand;
    }

    Car& operator = (const Car& object)
    {
        if (this != &object) // Check to prevent self-clone.
        {
            this->brand = object.brand;
        }
        return *this;
    }

    void GetBranch()
    {
        cout << "Manufacturer: " << brand << endl;
    }
};

int main()
{
    Car benz("Mercedes-Benz");
    benz.GetBranch();
    cout << "Object stored at " << &benz << endl;

    Car cloneOfBenz = benz;
    cloneOfBenz.GetBranch();
    cout << "Object clone stored at " << &cloneOfBenz << endl;
    return 0;
}
