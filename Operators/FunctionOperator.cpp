#include <iostream>
#include <string>
using namespace std;

/*
    Function operator makes objects behave like a function.
*/
class Display
{
public:
    void operator () (string input)
    {
        cout << "Content: " << input << endl;
    }
};

int main()
{
    Display display;
    display ("In thy light, I can see the light.");
    return 0;
}
