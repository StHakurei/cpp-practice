#include <iostream>
#include <string>
using namespace std;

/*
    A subscript operator makes easy to randomly access element of the array.
*/
class SecretNote
{
private:
    string* pointerOfNote;

public:
    SecretNote(const int length)
    {
        pointerOfNote = new string[length];
    }

    string& operator [] (int index)
    {
        return pointerOfNote[index];
    }

    string& operator [] (int index) const
    {
        return pointerOfNote[index];
    }
};

int main()
{
    SecretNote myNote(3);
    myNote[0] = "Galahad";
    myNote[1] = "Hakurei";
    myNote[2] = "Ishtar";
    cout << myNote[0] << endl;
    cout << myNote[1] << endl;
    cout << myNote[2] << endl;
    return 0;
}
