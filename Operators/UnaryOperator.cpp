#include <iostream>
using namespace std;

/*
    Image we have a storage service that will calculate customer's object storing time
    and show it with days and weeks.
    To add time elapsed per day through method or operator, and see the difference.
*/
class StorageService
{
private:
    int day, week;

public:
    StorageService()
    {
        day = 0;
        week = 0;
    }

    void Increment()
    {
        ++ day;
        if (day == 7)
        {
            day = 0;
            ++ week;
        }
    }

    StorageService& operator ++ ()
    {
        ++ day;
        if (day == 7)
        {
            day = 0;
            ++ week;
        }
        return *this;
    }

    void Report()
    {
        cout << "Service has running for " << day << " day " << week << " week." << endl;
    }
};

int main()
{
    StorageService object;

    // Let's assume that this service has running for 12 days.
    for (int i = 0; i < 12; i++)
    {
        object.Increment(); // Increment through method.
    }
    object.Report();

    // And continue running for 32 days.
    for (int i = 0; i < 32; i++)
    {
        /*
            In unary operator, use prefix operator like '++ object' instead of '-- object'
            to avoid the creation of a temporary copy that will not be used.    
        */
        ++ object; // Increment through operator.
    }
    object.Report();
    return 0;
}