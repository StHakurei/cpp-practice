#include <iostream>
using namespace std;

int main()
{
    /*
        It's better not assume everytime you can successfully allocate memroy,
        C++ provide two possible methods to ensure that your pointer is valid
        before using.

        For example:

        int* manyNumbers = new int [0x1ffffffffffff];

        This line of code will trigger below error in computer which has 16GB RAM.

        terminate called after throwing an instance of 'std::bad_alloc'
        what():  std::bad_alloc
        Aborted (core dumped)
    */
    try
    {
        int* manyNumbers = new int [0x1fffffff];
        cout << "[Method 1] - Successfully allocated memroies." << endl;
        delete[] manyNumbers;
    }
    catch (bad_alloc)
    {
        cout << "[Method 1] - Failed to allocate request memroy. Aborted." << endl;
    }
     
    // Another approach.
    int* manyIntegers = new(nothrow) int [0x1ffffffff];
    if (manyIntegers) // Check manyIntegers not equals to NULL.
    {
        cout << "[Method 2] - Successfully allocated memroies." << endl;
        delete[] manyIntegers;
    }
    else
    {
        cout << "[Method 2] - Failed to allocate request memroy. Aborted." << endl;
    }

    return 0;
}