#include <iostream>
using namespace std;

int main()
{
    /* In this sample code, program will change the stored data in target memory location. */
    int code = 100;
    int* pointerOfCode = &code;
    cout << "Initialized variable 'code' stored data " << code << " and is located at " << pointerOfCode << endl;

    cout << "Enter a new code:";
    cin >> *pointerOfCode;
    cout << "Initialized variable 'code' stored data " << code << " and is located at " << pointerOfCode << endl;

    /* Delete pointer after using. */
    delete pointerOfCode;
    return 0;
}