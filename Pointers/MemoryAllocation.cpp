#include <iostream>
#include <string.h>
#include <string>
using namespace std;

int main()
{
    /* Use operator 'new' to allocate memory. */
    string* pointerOfInt = new string;
    cout << "Allocate memory " << sizeof(*pointerOfInt) << " bytes" << endl;
    cout << "Address: " << pointerOfInt << endl;

    /* Use operator 'delete' to relase allocated memory. */
    delete pointerOfInt;

    /* Dynamically allocate memory. */
    cout << "How much int you want to declare?:";
    int num = 0;
    cin >> num;
    int* pointerOfNumbers = new int[num];
    cout << "Allocate memory for " << num << " integers at " << pointerOfNumbers << endl;

    /* Release allocated memory. */
    delete[] pointerOfNumbers;

    /*
        Memory Leaks.
        It's important to release allocated memory when done the job.

        int* pointers = new int[5];

        ... use pointers and forget to release by using delete[] pointers.

        pointers = new int[10];

        Leaks the previous allocated memory.
    */
    return 0;
}