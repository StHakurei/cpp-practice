#include <iostream>
using namespace std;

void Calculate(const double* pi, const double* radius)
{
    cout << "Arg pi = " << pi << endl;
    cout << "Arg radius = " << radius << endl;
    /* Check pointers before using! */
    if (pi && radius)
    {
        cout << "Aera = " << (*radius * 2 * *pi) << endl;
    }
    else
    {
        cout << "Invalid pointers!" << endl;
    }
}

int main()
{
    const double pi = 3.14;
    const double radius = 20.0;
    Calculate(&pi, &radius);
    return 0;
}