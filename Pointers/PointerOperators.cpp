#include <iostream>
using namespace std;

int main()
{
    /*
        Increment or Decrement of a pointer will not increase or decrease its size,
        but tell the compiler that you need to point to next value of the block of memory,
        see the output of below code.
    */
    int number = 100;
    int* pointerOfInt = &number;
    cout << pointerOfInt << " - Size: " << sizeof(pointerOfInt) << " bytes" << endl;

    ++pointerOfInt;
    cout << pointerOfInt << " - Size: " << sizeof(pointerOfInt) << " bytes" << endl;

    --pointerOfInt;
    cout << pointerOfInt << " - Size: " << sizeof(pointerOfInt) << " bytes" << endl;

    /* Here is the way to use it. */
    cout << "How many integer you want to store?:";
    int length = 0;
    cin >> length;
    
    // Allocate memory blocks.
    int* pointerOfStore = new int[length];
    
    // Store them by using pointer.
    for (int i = 0; i < length; i++)
    {
        cout << "Enter number " << i << ":";
        cin >> *(pointerOfStore + i);
    }
    
    // Output stored numbers.
    for (int i = 0; i < length; i++)
    {
        cout << "Number " << i << " is " << *(pointerOfStore + i) << " and stored at " << pointerOfStore + i << endl;
    }

    /* Delete pointer after using. */
    delete[] pointerOfStore;
    return 0;
}