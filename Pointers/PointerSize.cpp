#include <iostream>
using namespace std;

int main()
{
    /* Pointer is used to store the address of a variable, so its size is irrelevant to the size of it. */
    int code;
    char character;
    double rate;
    cout << "An empty Integer variable consume " << sizeof(code) << " bytes in memory." << endl;
    cout << "An empty Char variable consume " << sizeof(character) << " bytes in memory." << endl;
    cout << "An empty Double variable consume " << sizeof(rate) << " bytes in memory." << endl;

    int* pointerOfCode = &code;
    char* pointerOfChar = &character;
    double* pointerOfRate = &rate;
    cout << "The pointer of an empty Integer variable consume " << sizeof(pointerOfCode) << " bytes in memory." << endl;
    cout << "The pointer of an empty Char variable consume " << sizeof(pointerOfChar) << " bytes in memory." << endl;
    cout << "The pointer of an empty Double variable consume " << sizeof(pointerOfRate) << " bytes in memory." << endl;

    /* Delete pointer after using. */
    delete pointerOfCode;
    delete pointerOfChar;
    delete pointerOfRate;
    return 0;
}
