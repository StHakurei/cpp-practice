#include <iostream>
using namespace std;

int main()
{
    /* Declare an array of 3 integers. */
    int my_array[3];
    int* pointerOfArray = my_array;
    cout << "Address of index 0: " << &my_array[0] << endl;
    cout << "Pointer: " << pointerOfArray << endl;

    cout << "Address of index 1: " << &my_array[1] << endl;
    cout << "++Pointer: " << ++pointerOfArray << endl;

    cout << "Address of index 2: " << &my_array[2] << endl;
    cout << "++Pointer: " << ++pointerOfArray << endl;

    /* Remember delete pointer after using. */
    delete[] pointerOfArray;
    return 0;
}