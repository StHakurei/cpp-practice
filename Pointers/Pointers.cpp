#include <iostream>
using namespace std;

int main()
{
    /* Determining the address of a variable. */
    int code = 65565;
    double PI = 3.1415926;

    cout << "Integer variable " << code << " is located at: " << &code << endl;
    cout << "Double variable " << PI << " is located at: " << &PI << endl;

    /* Using pointer to store address. */
    int* pointerOfCode = &code;
    double* pointerOfPI = &PI;

    cout << "Pointer of Code stored: " << pointerOfCode << endl;
    cout << "Pointer of PI stored: " << pointerOfPI << endl;

    /* Access pointed data via dereference operator. */
    cout << "Pointer of Code pointed data: " << *pointerOfCode << endl;

    /* delete pointer after using. */
    delete pointerOfCode;
    delete pointerOfPI;
    return 0;
}