#include <iostream>
#include <string>
using namespace std;

/*
    Importance of pointer in C/C++:
    Pointers are used for accessing the resources which are external to the program like heap memory. 
    So for accessing the heap memory if anything is created inside heap memory Pointers are used.
*/

/*
    Problem with normal pointer:
*/
class Data
{
private:
	int num;
};

void HaveFun()
{
	Data* pointer = new Data();
}

/*
    Implementation of smart pointer.
*/
class SmartData
{
private:
	int* numPointer;

public:
	explicit SmartData(int* p = NULL)
	{
		numPointer = p;
	}

	~SmartData()
	{
		delete numPointer;
	}
};

int main()
{
	// Normal pointer.
	for (size_t i = 0; i < 100000; i++)
	{
		/*
		    Comment and uncomment these two lines of code to see the difference.
		    use command 'pmap [PID]' to check the RAM usage.
		*/
		HaveFun();
		//SmartData object(new int());
	}

	cout << "Press 'N' to exit:";
	string empty;
	cin >> empty;
	return 0;
}

/*
With normal pointer.

904827:   ./SmartPointer.out
000055e205409000      4K r---- SmartPointer.out
000055e20540a000      4K r-x-- SmartPointer.out
000055e20540b000      4K r---- SmartPointer.out
000055e20540c000      4K r---- SmartPointer.out
000055e20540d000      4K rw--- SmartPointer.out
000055e205fa6000   3300K rw---   [ anon ] <-- RAM usage for 100k object.
00007fd5d3383000     16K rw---   [ anon ]
00007fd5d3387000     60K r---- libm-2.31.so
00007fd5d3396000    668K r-x-- libm-2.31.so
00007fd5d343d000    604K r---- libm-2.31.so
00007fd5d34d4000      4K r---- libm-2.31.so
00007fd5d34d5000      4K rw--- libm-2.31.so
00007fd5d34d6000    148K r---- libc-2.31.so
00007fd5d34fb000   1504K r-x-- libc-2.31.so
00007fd5d3673000    296K r---- libc-2.31.so
00007fd5d36bd000      4K ----- libc-2.31.so
00007fd5d36be000     12K r---- libc-2.31.so
00007fd5d36c1000     12K rw--- libc-2.31.so
00007fd5d36c4000     16K rw---   [ anon ]
00007fd5d36c8000     12K r---- libgcc_s.so.1
00007fd5d36cb000     72K r-x-- libgcc_s.so.1
00007fd5d36dd000     16K r---- libgcc_s.so.1
00007fd5d36e1000      4K r---- libgcc_s.so.1
00007fd5d36e2000      4K rw--- libgcc_s.so.1
00007fd5d36e3000    600K r---- libstdc++.so.6.0.28
00007fd5d3779000    964K r-x-- libstdc++.so.6.0.28
00007fd5d386a000    292K r---- libstdc++.so.6.0.28
00007fd5d38b3000      4K ----- libstdc++.so.6.0.28
00007fd5d38b4000     44K r---- libstdc++.so.6.0.28
00007fd5d38bf000     12K rw--- libstdc++.so.6.0.28
00007fd5d38c2000     20K rw---   [ anon ]
00007fd5d38e6000      4K r---- ld-2.31.so
00007fd5d38e7000    140K r-x-- ld-2.31.so
00007fd5d390a000     32K r---- ld-2.31.so
00007fd5d3913000      4K r---- ld-2.31.so
00007fd5d3914000      4K rw--- ld-2.31.so
00007fd5d3915000      4K rw---   [ anon ]
00007fff3b542000    132K rw---   [ stack ]
00007fff3b583000     16K r----   [ anon ]
00007fff3b587000      8K r-x--   [ anon ]
ffffffffff600000      4K --x--   [ anon ]
 total             9060K

With smart pointer.

906525:   ./SmartPointer.out
000055b2a8622000      4K r---- SmartPointer.out
000055b2a8623000      4K r-x-- SmartPointer.out
000055b2a8624000      4K r---- SmartPointer.out
000055b2a8625000      4K r---- SmartPointer.out
000055b2a8626000      4K rw--- SmartPointer.out
000055b2aa3a8000    132K rw---   [ anon ] <-- RAM usage for 100k object.
00007f3c36075000     16K rw---   [ anon ]
00007f3c36079000     60K r---- libm-2.31.so
00007f3c36088000    668K r-x-- libm-2.31.so
00007f3c3612f000    604K r---- libm-2.31.so
00007f3c361c6000      4K r---- libm-2.31.so
00007f3c361c7000      4K rw--- libm-2.31.so
00007f3c361c8000    148K r---- libc-2.31.so
00007f3c361ed000   1504K r-x-- libc-2.31.so
00007f3c36365000    296K r---- libc-2.31.so
00007f3c363af000      4K ----- libc-2.31.so
00007f3c363b0000     12K r---- libc-2.31.so
00007f3c363b3000     12K rw--- libc-2.31.so
00007f3c363b6000     16K rw---   [ anon ]
00007f3c363ba000     12K r---- libgcc_s.so.1
00007f3c363bd000     72K r-x-- libgcc_s.so.1
00007f3c363cf000     16K r---- libgcc_s.so.1
00007f3c363d3000      4K r---- libgcc_s.so.1
00007f3c363d4000      4K rw--- libgcc_s.so.1
00007f3c363d5000    600K r---- libstdc++.so.6.0.28
00007f3c3646b000    964K r-x-- libstdc++.so.6.0.28
00007f3c3655c000    292K r---- libstdc++.so.6.0.28
00007f3c365a5000      4K ----- libstdc++.so.6.0.28
00007f3c365a6000     44K r---- libstdc++.so.6.0.28
00007f3c365b1000     12K rw--- libstdc++.so.6.0.28
00007f3c365b4000     20K rw---   [ anon ]
00007f3c365d8000      4K r---- ld-2.31.so
00007f3c365d9000    140K r-x-- ld-2.31.so
00007f3c365fc000     32K r---- ld-2.31.so
00007f3c36605000      4K r---- ld-2.31.so
00007f3c36606000      4K rw--- ld-2.31.so
00007f3c36607000      4K rw---   [ anon ]
00007fff66119000    132K rw---   [ stack ]
00007fff6616f000     16K r----   [ anon ]
00007fff66173000      8K r-x--   [ anon ]
ffffffffff600000      4K --x--   [ anon ]
 total             5892K

*/