#include <iostream>
#include <string.h>
using namespace std;

class Prototype
{
public:
    int code;
    virtual void GetCode() = 0;
    virtual void SetCode(int code) = 0;
};

class Newtype: public Prototype
{
public:
    /*
        This specifier indicates that this method overrides the method of base class,
        and compiler will check it to ensure the override is successful.
    */
    void GetCode() override
    {
        cout << code << endl;
    }

    /*
        This specifier will pervent any further specilization of this virtual method.
    */
    void SetCode(int code) override final
    {
        this->code = code;
    }
};

class DuplicateType: public Newtype
{
    /*
        Compiler will prompt error message about trying to override a final method.

        void SetCode(int code)
        {
            this->code = code + 100;
        }

        Error message below:
        IntentionalOverride.cpp:36:10: error: virtual function ‘virtual void DuplicateType::SetCode(int)’ overriding final function
           36 |     void SetCode(int code)
              |          ^~~~~~~
        IntentionalOverride.cpp:28:10: note: overridden function is ‘virtual void Newtype::SetCode(int)’
           28 |     void SetCode(int code) override final
              |          ^~~~~~~
    */
};

int main()
{
    Newtype object;
    object.SetCode(100);
    object.GetCode();

    DuplicateType object1;
    object1.SetCode(200);
    object1.GetCode();
    return 0;
}