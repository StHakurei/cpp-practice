#include <iostream>
#include <string.h>
using namespace std;

/*
    Pure virtual function declaration essentially tells the compiler that
    the functions needs to be implemented and by the class that derives
    from base class.
*/
class Airplane
{
public:
    virtual void GetSpecification() = 0;
};

class Airbus: public Airplane
{
public:
    /*
        If you don't override this function, you will get error message from compiler.

        PureVirtualFunction.cpp: In function ‘int main()’:
        PureVirtualFunction.cpp:30:12: error: cannot declare variable ‘a350’ to be of abstract type ‘Airbus’
           30 |     Airbus a350;
              |            ^~~~
        PureVirtualFunction.cpp:16:7: note:   because the following virtual functions are pure within ‘Airbus’:
           16 | class Airbus: public Airplane
              |       ^~~~~~
        PureVirtualFunction.cpp:13:18: note:    ‘virtual void Airplane::GetSpecification()’
           13 |     virtual void GetSpecification() = 0;
              |                  ^~~~~~~~~~~~~~~~
    */
    void GetSpecification()
    {
        cout << "Airbus product." << endl;
    }
};

int main()
{
    Airbus a350;
    a350.GetSpecification();
    return 0;
}

