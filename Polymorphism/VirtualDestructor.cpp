#include <iostream>
#include <string.h>
using namespace std;

/*
    This sample will demostrate why virtual destructor is needed.
    Bad sample in the line of code 9 to 47 (DON'T do that in your code).
    Good sample in the line of code 
*/
class Prototype
{
private:
    string* pointerOfPrototype = NULL;
public:
    Prototype()
    {
        cout << "Proto constructor." << endl;
        pointerOfPrototype = new string;
    }

    ~Prototype()
    {
        cout << "Proto destructor." << endl;
        delete pointerOfPrototype;
    }
};

class Newtype: public Prototype
{
private:
    string* pointerOfNewtype = NULL;
public:
    Newtype()
    {
        cout << "Newtype constructor." << endl;
        pointerOfNewtype = new string;
    }

    ~Newtype()
    {
        delete pointerOfNewtype;
    }
};

void ReleaseMemory(Prototype* object)
{
    delete object;
}

// Good sample.
class Airplane
{
private:
    string* manufacturer;

public:
    Airplane()
    {
        cout << "Invoking Airplane constructor..." << endl;
        manufacturer = new string;
    }

    virtual ~Airplane()
    {
        cout << "Invoking Airplane destructor..." << endl;
        delete manufacturer;
    }
};

class Airbus: public Airplane
{
private:
    string* model;

public:
    Airbus()
    {
        cout << "Invoking Airbus constructor..." << endl;
        model = new string;
    }

    ~Airbus()
    {
        cout << "Invoking Airbus destructor..." << endl;
        delete model;
    }
};

void DeprecateAirplane(Airplane* object)
{
    delete object;
}

int main()
{
    Newtype* object0 = new Newtype;
    ReleaseMemory(object0);
    /*
        Above sample code will output:

        Proto constructor.
        Newtype constructor.
        Proto destructor.

        You can see that the destructor of class 'Newtype' hasn't been invoked,
        this can result in resources not being released and memory leaks.

        But virtual destructor will help you solve it easily.
    */

    Airbus* a350 = new Airbus;
    DeprecateAirplane(a350);
    return 0;
}