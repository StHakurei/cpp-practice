#include <iostream>
#include <string.h>
using namespace std;

/*
    Virtual function could let you treat different object as a common one.
*/
class Airplane
{
public:
    virtual void GetSpecification()
    {
        cout << "Prototype airplane." << endl;
    }
};

class Airbus: public Airplane
{
public:
    string manufacturer = "Airbus";
    void GetSpecification() // If you don't override the function, the prototype will be invoked.
    {
        cout << manufacturer << " airplane." << endl;
    }
};

class Boeing: public Airplane
{
public:
    string manufacturer = "Boeing";
    void GetSpecification()
    {
        cout << manufacturer << " airplane." << endl;
    }
};

// The function take common type as its parameter.
void DisplaySpecification(Airplane& ariplane)
{
    ariplane.GetSpecification();
}

int main()
{
    Airbus a350;
    Boeing b787;
    DisplaySpecification(a350);
    DisplaySpecification(b787);
    return 0;
}

