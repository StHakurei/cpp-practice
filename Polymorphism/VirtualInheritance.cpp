#include <iostream>
#include <string.h>
using namespace std;

class Engine
{
public:
    int pushInKn;

    Engine()
    {
        cout << "Engine constructor." << endl;
    }
};

class Turboprop: public Engine
{
    // Leave blank intentionally.
};

class Turbojet: public Engine
{
    // Leave blank intentionally.
};

class Turbofan: public Engine
{
    // Leave blank intentionally.
};

class GEEngine: public Turboprop, public Turbojet, public Turbofan
{
public:
    GEEngine()
    {
        cout << "GEEngine constructor." << endl;
    }
};

class TurbopropEngine: public virtual Engine
{
    // Leave blank intentionally.
};

class TurbojetEngine: public virtual Engine
{
    // Leave blank intentionally.
};

class TurbofanEngine: public virtual Engine
{
    // Leave blank intentionally.
};

class RollsRoyceEngine final: public TurbopropEngine, public TurbojetEngine, public TurbofanEngine
{
public:
    RollsRoyceEngine()
    {
        cout << "Rolls-Royce constructor." << endl;
    }
};

int main()
{
    GEEngine object;
    /*
        object.pushInKn = 63000;

        This line of code will let compiler return an error message about attribute 'pushInKn',
        because compiler doesn't know which 'pushInKn' you want to set, it exists in all base
        classes. Unless you want to do it in another ridiculous way.
    */
    object.Turbofan::pushInKn = 630;
    cout << "Max throttle push is " << object.Turbofan::pushInKn << " Kn." << endl; 

    // Virtual inheritance could help you solve in convenience.
    RollsRoyceEngine trentXWB;
    trentXWB.pushInKn = 431;
    cout << "Max throttle push is " << trentXWB.pushInKn << " Kn." << endl; 
    return 0;
}