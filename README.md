## C++ Practice

My C++ practice code archive.

```bash
./cpp-practice/
├── ArrayBasics 
├── CastingOperators
├── Classes&Objects
├── Conditions
├── Enumerations
├── ExceptionHandling
├── Expressions&Statements&Operators
├── FunctionBasics
├── Inheritance&Implementing
├── Macros&Templates
├── Operators
├── Pointers 
├── Polymorphism
├── References
├── STL-Algorithm
├── STL-StandardTemplateLibrary
├── Stream
├── Strings
└── VariableBasics
```

## Writing Great C++ Code

- **Give your variable name that make sence.**
- **Always initialize variables such as int, float and the like.**
- **Always intiialize pointer to NULL.**
- **When using arrays, never cross the bounds of the array buffer.**
- **Use static array only when you are certain of the number of elements will be contained.**
- **C++ string is safer and provides many useful utility methods than char\* string buffer.**
- **When declaring and definning functions that like NON-POD (plain old data), consider declaring parameters as reference parameters to avoid the unnecessary copy step when the function is called.**
- **Consider programming copy constructor and copy assignment operator when your class contains a raw pointer.**
- **Avoid using raw-pointers. Chooes the approriate smart pointers where possible.**
- **If your lambda function goes to large, consider making a function instead.**
- **Resource allocation should always be made exception safe**
- **Never throw from the destructor of a class.**
