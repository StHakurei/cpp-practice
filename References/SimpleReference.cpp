#include <iostream>
using namespace std;

void GetPerimeter(double& perimeter, double radius)
{
    perimeter = radius * 2 * 3.14;
}

int main()
{
    // Reference is a simple alias of a vairable.
    int origin = 100;
    int& ref1 = origin;
    int& ref2 = ref1;
    
    // You could see all variable are actually stored at the same memory location.
    cout << "The original number " << origin << " - located at: " << &origin << endl;
    cout << "The first reference " << ref1 << " - located at: " << &ref1 << endl;
    cout << "The second reference " << ref2 << " - located at: " << &ref2 << endl;

    // Use reference in function to manipulate variable without return value.
    double radius = 0;
    double perimeter = 0;
    cout << "Give an radius:";
    cin >> radius;
    GetPerimeter(perimeter, radius);
    cout << "The perimeter is " << perimeter << endl;

    // You may want to not allow reference change the value of origin, use keyword "const".
    int corner = 1000;
    const int& reference1 = corner;
    /*
        reference1 = 100;

        Abobe line of code will trigger the error:

        SimpleReference.cpp: In function ‘int main()’:
        SimpleReference.cpp:32:16: error: assignment of read-only reference ‘reference1’
           32 |     reference1 = 100; // It's not allowed.
              |     ~~~~~~~~~~~^~~~~


        int& reference2 = reference1;

        Above line of code will trigger the error:

        SimpleReference.cpp: In function ‘int main()’:
        SimpleReference.cpp:42:23: error: binding reference of type ‘int&’ to ‘const int’ discards qualifiers
        42 |     int& reference2 = reference1;
           |                       ^~~~~~~~~~
    */
    const int& reference2 = reference1;
    cout << "The corner " << corner << " - located at: " << &corner << endl;
    cout << "Reference1 " << reference1 << " - located at: " << &reference1 << endl;
    cout << "Reference2 " << reference2 << " - located at: " << &reference2 << endl;
    return 0;
}