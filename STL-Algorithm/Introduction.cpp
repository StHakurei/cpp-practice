#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <algorithm>
#include <ctime>
using namespace std;

/*
    STL algoritms can be boardly classified into two types: non-mutating and mutating.

    Non-mutating Algorithm: Change neither the order nor the content of a container.
    Mutating Algorithm: Change the order or the content of a container.
*/
template <typename T>
void DisplayCollection(const T& container)
{
    for (auto i = container.cbegin(); i != container.cend(); ++i)
    {
        cout << *i << " ";
    }
    cout << endl;
}

// Non-mutating algorithm below.
void FindAlgorithm()
{
    vector<int> sample {-1, 20, 100, -40, -17};
    cout << "** FindAlgorithm **" << endl;
    cout << "Output sample vector content: ";
    DisplayCollection(sample);

    // Find elements by a given value or condition.
    auto element0 = find(sample.cbegin(), sample.cend(), 100);
    auto element1 = find_if(sample.cbegin(), // Start range.
                            sample.cend(),  // End range.
                            [](int element){ return (element % 2) == 0;}); // A lambda express.

    if (element0 != sample.cend())
    {
        cout << "Found number " << *element0 << " in the vector at the position " << distance(sample.cbegin(), element0) << endl;
    }
    else
    {
        cout << "Element was not found." << endl;
    }

    if (element1 != sample.cend())
    {
        cout << "Found number " << *element1 <<  " in the vector ar the position " << distance(sample.cbegin(), element1) << endl;
    }
    else
    {
        cout << "Element was not found." << endl;
    }
}

template <typename T>
bool IsEven(const T& number)
{
    return ((number % 2) == 0);
}

template <typename T>
bool IsOdd(const T& number)
{
    return ((number % 2) == 1);
}

void CountingAlgorithm()
{
    vector<int> sample {0, 25 , 32, 38, 87, 95, 64, 76, 0};
    cout << "** CountingAlgorithm **" << endl;
    cout << "Output sample vector content: ";
    DisplayCollection(sample);

    // Count elements by a given value or condition.
    size_t quantity0 = count(sample.cbegin(), sample.cend(), 0);
    size_t quantity1 = count_if(sample.cbegin(), sample.cend(), IsEven<int>);
    size_t quantity2 = count_if(sample.cbegin(), sample.cend(), IsOdd<int>);
    cout << "The number of instance of '0': " << quantity0 << endl; 
    cout << "The number of instance of Even Number: " << quantity1 << endl; 
    cout << "The number of instance of Odd Number: " << quantity2 << endl; 
}

void SearchingAlgorithm()
{
    vector<int> sample {-100, -200, 0, 100, 200, 300, 400, 23, 23, 23};
    list<int> range {100, 200, 300};
    cout << "** SearchingAlgorithm **" << endl;
    cout << "Output sample vector content: ";
    DisplayCollection(sample);
    cout << "Output range list content: ";
    DisplayCollection(range);

    // Search an element or a range in a Collection.
    auto range0 = search(sample.cbegin(),
                           sample.cend(),
                           range.cbegin(),
                           range.cend());
    auto range1 = search_n(sample.cbegin(), sample.cend(), 3, 23);
    if (range0 != sample.cend())
    {
        cout << "Found {100, 200, 300} at the position: " << distance(sample.cbegin(), range0) << endl;
    }
    else
    {
        cout << "Not found." << endl;
    }

    if (range1 != sample.cend())
    {
        cout << "Found {23, 23, 23} at the position: " << distance(sample.cbegin(), range1) << endl;
    }
    else
    {
        cout << "Not found." << endl;
    }
}

// Mutating algorithm below.
void InitializingAlgorithm()
{
    cout << "** InitializingAlgorithm **" << endl;
    vector<int> sample(3); // Initialize the vector with 3 elements.
    fill(sample.begin(), sample.end(), 100); // Fill all elements in the container with value 100.
    DisplayCollection(sample);

    sample.resize(6); // Resize vector with 6 elements.
    fill_n(sample.begin() + 3, 3, -100); // Fill the the elements from the offset 3 with value -100.
    DisplayCollection(sample);

    sample.resize(9);
    fill_n(sample.begin() + 6, 3, 9);
    DisplayCollection(sample);
}

void GenerateAlgorithm()
{
    cout << "** GenerateAlgorithm **" << endl;
    srand(time(NULL)); // Seed random generator by time.
    list<int> sample(10);
    generate(sample.begin(), sample.end(), rand); // Fill list with random numbers.
    DisplayCollection(sample);

    list<int> sample1(5);
    generate_n(sample1.begin(), 3, rand); // Fill the first 3 elements of the list with random numbers.
    DisplayCollection(sample1);
}

void ForEachAlgorithm()
{
    cout << "** For_each Algorithm **" << endl;
    string words ("In thy light, I can see the light.");
    int numChars = 0;
    for_each (words.cbegin(), words.cend(), [&numChars](char c) {cout << c << " ";});
    cout << endl;
}

void TransformAlgorithm()
{
    cout << "** Transform Algorithm **" << endl;
    string words ("In thy light, I can see the light.");
    string upperWords;
    upperWords.resize(words.size());
    // Use transform to convert string to upper case.
    transform(words.cbegin(), words.cend(), upperWords.begin(), ::toupper); // Unary function.
    cout << "Transform to upper case." << endl;
    DisplayCollection(words);
    DisplayCollection(upperWords);

    vector<int> original {10, 20, 30, 40, 50};
    vector<int> copyOfOriginal(original.size(), -2);
    deque<int> result(original.size());
    transform(original.cbegin(), original.cend(), copyOfOriginal.cbegin(), result.begin(), plus<int>());
    cout << "Performing calculation." << endl;
    DisplayCollection(original);
    DisplayCollection(copyOfOriginal);
    DisplayCollection(result);    
}

void CopingAndRemovalAlgorithm()
{
    cout << "** Copy & Remove Algorithm **" << endl;
    vector<int> sample {10, 11, 20, 21, 30, 31, 0, 0, 0};
    vector<int> another(sample.size() * 2);
    auto copy1 = copy(sample.cbegin(), sample.cend(), another.begin());
    DisplayCollection(sample); 
    DisplayCollection(another);

    vector<int> odd(sample.size());
    // use "copy_backward" to assigns contents to the destination range in the backward direction.
    auto copy2 = copy_if(sample.cbegin(), sample.cend(), odd.begin(), [](int element){return ((element % 2) != 0);});
    DisplayCollection(odd);
    
    // Remove all '0' elements.
    auto removal = remove(sample.begin(), sample.end(), 0);
    sample.erase(removal, sample.end());
    DisplayCollection(sample);
    
    // Remove all odd numbers.
    auto oddRemoval = remove_if(sample.begin(), sample.end(), [](int element){return ((element % 2) != 0);});
    sample.erase(oddRemoval, sample.end());
    DisplayCollection(sample);
}

void ReplacingAlgorithm()
{
    cout << "** ReplacingAlgorithm **" << endl;
    vector<int> sample {10, 10, 10, 11, 20, 21, 30, 31};
    DisplayCollection(sample);

    // Replace 10 by 100.
    replace(sample.begin(), sample.end(), 10, 100);
    DisplayCollection(sample);

    // Replace odd numbers by -1.
    replace_if(sample.begin(), sample.end(), [](int element){return ((element % 2) != 0);}, -1);
    DisplayCollection(sample);
}

void SortingAlgorithm()
{
    cout << "** SortingAlgorithm **" << endl;
    vector<int> sample {1, -2, 23, -234, 34, 78, -23, -2, 34, -2};
    DisplayCollection(sample);

    // Sort vector.
    sort(sample.begin(), sample.end());
    DisplayCollection(sample);

    // binary_search number 34.
    bool found = binary_search(sample.begin(), sample.end(), 34);
    if (found)
    {
        cout << "Number 34 in the container." << endl;
    }
    else
    {
        cout << "Not found." << endl;
    }

    // Removal duplication.
    auto duplicated = unique(sample.begin(), sample.end());
    sample.erase(duplicated, sample.end());
    DisplayCollection(sample);
}

void PartitionAlgorithm()
{
    cout << "** PartitionAlgorithm **" << endl;
    vector<int> sample {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    cout << "Original vector:" << endl;
    DisplayCollection(sample);

    vector<int> copyOfSample(sample);

    cout << "Effection of using partition():" << endl;
    partition(sample.begin(), sample.end(), [](int element){return ((element % 2) == 0);});
    DisplayCollection(sample);

    cout << "Effection of using statble_partition():" << endl;
    stable_partition(copyOfSample.begin(), copyOfSample.end(), [](int element){return ((element % 2) == 0);});
    DisplayCollection(copyOfSample);
}

void InsertionAlgorithm()
{
    cout << "** InsertionAlgorithm **" << endl;
    list<int> sample {3, 6, 2, 4, 9, 0, 5, 1, 7, 8};
    cout << "Original list:" << endl;
    DisplayCollection(sample);

    sample.sort();
    cout << "Sorted:" << endl;
    DisplayCollection(sample);

    auto minIndex = lower_bound(sample.begin(), sample.end(), 6);
    auto maxIndex = upper_bound(sample.begin(), sample.end(), 6);
    cout << "The lowest postion of where '6' can be inserted: " << distance(sample.begin(), minIndex) << endl;
    cout << "The highest postion of where '6' can be inserted: " << distance(sample.begin(), maxIndex) << endl;

    cout << "Insert to the lowest postion." << endl;
    sample.insert(maxIndex, 6);
    DisplayCollection(sample);
}

int main()
{
    cout << "=== Non-mutating algorithm ===" << endl;
    FindAlgorithm();
    CountingAlgorithm();
    SearchingAlgorithm();
    cout << endl;

    cout << "=== Mutating algorithm ===" << endl;
    InitializingAlgorithm();
    GenerateAlgorithm();
    ForEachAlgorithm();
    TransformAlgorithm();
    CopingAndRemovalAlgorithm();
    ReplacingAlgorithm();
    SortingAlgorithm();
    PartitionAlgorithm();
    InsertionAlgorithm();
    return 0;
}