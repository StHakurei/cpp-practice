#include <iostream>
#include <string>
#include <vector>
#include <bitset>
using namespace std;

void ShowAddress(const vector<bitset<8>>& container)
{
    for (auto i = container.begin(); i != container.end(); ++i)
    {
        cout << *i;
        if (i != (-- container.end()))
        {
            cout << ".";
        }
    }
    cout << endl;
}

void ShowAND(const vector<bitset<8>>& container0, const vector<bitset<8>>& container1)
{
    for (size_t i = 0; i < 4; ++i)
    {
        cout << (container0[i] & container1[i]);
        if (i < 3)
        {
            cout << ".";
        }
    }
    cout << endl;
}

int main()
{
    bitset<2> twoBits;
    cout << "Initialized two bits to 00: " << twoBits << endl;

    bitset<4> fourBits("0101");
    cout << "Initialized four bits to 0101: " << fourBits << endl;

    bitset<8> eightBits(255);
    cout << "Initialized eight bits with long int 255: " << eightBits << endl;

    /*
        IP address "AND" calculation by using bitset and vector.
    */
    vector<bitset<8>> netAddress(4, 0);
    netAddress[0] = bitset<8>(192);
    netAddress[1] = bitset<8>(168);
    netAddress[2] = bitset<8>(100);
    netAddress[3] = bitset<8>(0);
    cout << "Show network address 192.168.100.1 in bits:" << endl;
    ShowAddress(netAddress);

    vector<bitset<8>> netMask(4, 0);
    netMask[0] = bitset<8>(255);
    netMask[1] = bitset<8>(255);
    netMask[2] = bitset<8>(255);
    netMask[3] = bitset<8>(0);
    cout << "Show network mask 255.255.255.0 in bits." << endl;
    ShowAddress(netMask);

    vector<bitset<8>> ipAddress(4, 0);
    ipAddress[0] = bitset<8>(192);
    ipAddress[1] = bitset<8>(168);
    ipAddress[2] = bitset<8>(100);
    ipAddress[3] = bitset<8>(15);
    cout << "Show IP address 192.168.100.15 in bits." << endl;
    ShowAddress(ipAddress);

    cout << "Show result of AND calculation of network address and network mask:" << endl;
    ShowAND(netAddress, netMask);
    cout << "Show result of AND calculation of ip address and network mask:" << endl;
    ShowAND(ipAddress, netMask);
    cout << "Results are same, some this ip address belongs to this network." << endl;

    vector<bitset<8>> anotherIp(4, 0);
    anotherIp[0] = bitset<8>(192);
    anotherIp[1] = bitset<8>(168);
    anotherIp[2] = bitset<8>(200);
    anotherIp[3] = bitset<8>(15);
    cout << "Show another IP address 192.168.200.15 in bits." << endl;
    ShowAddress(anotherIp);

    cout << "Show result of AND calculation of network address and network mask:" << endl;
    ShowAND(netAddress, netMask);
    cout << "Show result of AND calculation of another ip address and network mask:" << endl;
    ShowAND(anotherIp, netMask);
    cout << "Results are not same, some this ip address is not in this network." << endl;
    return 0;
}