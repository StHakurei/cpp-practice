#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
using namespace std;

void DisplayArray(const vector<int>& input)
{
    for (auto i = input.begin(); i != input.end(); ++ i)
    {
        cout << *i << " ";
    }
    cout << endl;
}

void DisplayArray(const deque<int>& input)
{
    for (size_t index = 0; index < input.size(); ++ index)
    {
        cout << input[index] << " ";
    }
    cout << endl;
}

int main()
{
    /*
        vector is a template class that supplies generic functionality of a dymanic array.
    */
    // Initialize integer array.
    vector<int> array0;

    // Initialize integer array by using C++11 list initialization.
    vector<int> array1 {100, 200, 300, 400, 500};

    // Initialize integer array with 10 elements, it can still grow.
    vector<int> array2 (10);

    // Initialize integer array with 10 elements, each initialized to 100.
    vector<int> array3 (10, 100);

    // Initialize integer array to the contents of another.
    vector<int> copyOfArray3 (array3);

    // Initialize integer array to elements from another using iterators.
    vector<int> copyOfArray1 (array1.cbegin(), array1.cbegin() + 3);

    /*
        vector common operator.
    */
    // Insert element to the end of array.
    array0.push_back(50);
    array0.push_back(60);
    array0.push_back(70);

    cout << "The array0 contains ";
    cout << array0.size() << " elements." << endl;

    /*
        Insert element at the given postion of an array.
    */
    // Insert element to the start of an array.
    array2.insert(array2.begin(), 2);
    array2.insert(array2.begin(), 3);
    array2.insert(array2.begin(), 4);
    DisplayArray(array2);

    // Insert element ot the given postions of an array.
    DisplayArray(array3);
    array3.insert(array3.begin() + 1, 11);
    array3.insert(array3.begin() + 3, 12);
    array3.insert(array3.begin() + 5, 13);
    DisplayArray(array3);

    // Insert two elements into the given postion of an array.
    array3.insert(array3.begin() + 7, 2, 255);
    DisplayArray(array3);

    /*
        Accessing elements by using array semantics.
    */
    cout << "Before" << endl;
    DisplayArray(copyOfArray1);
    copyOfArray1[0] = 0;
    copyOfArray1[1] = 1;
    copyOfArray1[2] = 2;
    cout << "After" << endl;
    DisplayArray(copyOfArray1);

    /*
        Accessing elements in a vector using [] operator is fraught with same dangers as accessing
        elements in an array, you will not get notification when acorss the bound of container.\
        A safer alternative is to use 'at()' member function, you will get 'std::out_of_range' when
        acorss bound of the array.
    */
    cout << copyOfArray1[100] << endl;
    cout << copyOfArray1.at(2) << endl;

    /*
        Remeving element from array.
    */
    // Erase element from the end of an array.
    DisplayArray(array1);
    array1.pop_back();
    DisplayArray(array1);
    
    // Understand the concepts of Size and Capacity.
    cout << "Size of array1: " << array1.size() << endl;
    cout << "Capacity of array1: " << array1.capacity() << endl;
    array1.push_back(100);
    cout << "Pushed an element into vector." << endl;
    cout << "Size of array1: " << array1.size() << endl;
    cout << "Capacity of array1: " << array1.capacity() << endl;
    array1.push_back(100);
    cout << "Pushed another into vector." << endl;
    cout << "Size of array1: " << array1.size() << endl;
    cout << "Capacity of array1: " << array1.capacity() << endl;
    array1.push_back(100);
    cout << "Pushed another into vector." << endl;
    cout << "Size of array1: " << array1.size() << endl;
    cout << "Capacity of array1: " << array1.capacity() << endl;

    /*
        The STL deque class.
        Deque allows for the insertion and removal of elements at the front and back of the array.
    */
    cout << "Deque sample." << endl;
    deque<int> intDeque;

    // Add elements to the front.
    cout << "Push elements to the front" << endl;
    intDeque.push_front(10);
    intDeque.push_front(20);
    intDeque.push_front(30);
    DisplayArray(intDeque);

    // Add elements to the back.
    cout << "Push elements to the back" << endl;
    intDeque.push_back(100);
    intDeque.push_back(200);
    intDeque.push_back(300);
    DisplayArray(intDeque);

    cout << "Erase one element from the front and back." << endl;
    intDeque.pop_front();
    intDeque.pop_back();
    DisplayArray(intDeque);
    return 0;
}