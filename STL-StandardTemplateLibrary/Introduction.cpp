#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

/*
    Standard Template Library (STL) is a set of template classes and functions that supply the
    programmer with below purpose.

    - Containers for storing information.
    - Iterators for accessing information.
    - Algorithm for manipulating the content of containers.

    About Container:

    STL supplies two type of container classes.

    - Squential containers: used to hold data in a sequential fashion, such as Array and List.
    - Associative containers: used to hold data in a sorted fashion, like a dictionary, this results in slower insertion times,
                              but presents signifcant advantages when it comes to searching.

    Container adapters:
    std::stack - Store elements in a  LIFO (last in first out) fashion, allowing elements to be inserted (pushed) and removed (popped) 
                 at the top.
    std::queue - Store elements in a FIFO (first in first out) fashion, allowing the first element to be removed in the order of
                 they are inserted.
    std::priority_queue - Store elements like a queue but some element is evaluated to be the hightest is always in the first position.
    etc.

    About Iterator:

    ???

    About Algorithm:

    STL supplies most used standard algorithm that not required the programmers to reinvent implementation to support, such as
    std::find - Find a value in collection.
    std::find_if - Find a value in a collection on the basis of a specific user-defined predicate.
    std::reverse - Reverse a collection.
    std::remove_if - Remove a vlue in a collection on the basis of a specific user-defined predicate.
    std::transform - Apply a user-defined transformation function to elements in a collection.
*/

int main()
{
    // Declare a dynamic array of integers.
    vector<int> intArray; // C++ STL Sequential Container. Use container adapter std::vector.

    // Insert elements into the array.
    intArray.push_back(100);
    intArray.push_back(50);
    intArray.push_back(75);
    intArray.push_back(25);

    cout << "The contents of the vector are: " << endl;

    // Walk the vector and read values using an iterator.
    vector <int>::iterator arrayInterator = intArray.begin();
    
    while (arrayInterator != intArray.end())
    {
        cout << *arrayInterator << endl;
        ++ arrayInterator;
    }

    // Find an element using the STL "find" algorithm.
    vector <int>::iterator findInterator = find(intArray.begin(), intArray.end(), 25);

    if (findInterator != intArray.end())
    {
        int index = distance(intArray.begin(), findInterator);
        cout << "Value " << *findInterator;
        cout << " at Index " << index << endl;
    }
    else
    {
        cout << "Didn't find this element." << endl;
    }

    return 0;
}