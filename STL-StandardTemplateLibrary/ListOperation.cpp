#include <list>
#include <forward_list>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

template <typename T>
void DisplayList(const T& input)
{
    for (auto i = input.cbegin(); i != input.cend(); ++ i)
    {
    	cout << *i << " ";
    }
    cout << endl;
}

void InsertElement(list<int>* input, const int index, const int value)
{
	auto anker = input->cbegin();
	for (size_t pos = 0; pos < index; ++ pos)
	{
		++ anker;
	}
	input->insert(anker, value);
}

template <typename T>
void EraseElement(T* input, const int index)
{
	if (index < input->size())
	{
		auto anker = input->cbegin();
	    for (size_t pos = 0; pos < index; ++ pos)
	    {
	    	++ anker;
	    }
	    input->erase(anker);
	}
	else
	{
		cout << "Invalid index." << endl;
	}
}

int main()
{
	/* 
	    The ways of instantiate lists.
	*/
	// Create an empty list.
	list<int> sample0;

	// Create a list with 10 integers.
	list<int> sample1(10);  

	// Create a list with 10 integers and set initial value as 100.
	list<int> sample2(10, 100);

	// Create a copy of an existing list.
	list<int> copyOfSample2(sample2);

	// Create a list by using values from another container.
	vector<int> array0(10, 255);
	list<int> copyOfArray0(array0.cbegin(), array0.cend());

	/* 
	    Inserting elements from the front and the back of a list.
	*/
    
    // Instantiate list with 2 integers.
	list<int> linkedList {100, -100};
	cout << "Before:" << endl;
	DisplayList(linkedList);

    // Insert elements.
	linkedList.push_front(50);
	linkedList.push_front(25);
	linkedList.push_back(-50);
	linkedList.push_back(-25);
	cout << "After:" << endl;
	DisplayList(linkedList);

	/*
	    Inserting elements to the middle of a list.
	*/
	list<int> linkedList1;
	linkedList1.insert(linkedList1.begin(), 100);
	linkedList1.insert(linkedList1.end(), -100);
	cout << "Before:" << endl;
	DisplayList(linkedList1);
	cout << "List size = " << linkedList1.size() << endl;

	// Insert element to given position.
	InsertElement(&linkedList1, 1, -19);
	cout << "After:" << endl;
	DisplayList(linkedList1);
    cout << "List size = " << linkedList1.size() << endl;

	/*
	    Erase elements from the list.
	*/
	list<string> linkedList2 {"Peter", "Lisa", "Andrew", "Ishtar", "Ereshkigal"};
	cout << "Before:" << endl;
	DisplayList(linkedList2);
	cout << "List size = " << linkedList2.size() << endl;

	// Store an iterator obtained in using insert().
	auto pos = linkedList2.insert(linkedList2.begin(), "Ozymandias");
	DisplayList(linkedList2);
	cout << "List size = " << linkedList2.size() << endl;
	linkedList2.erase(pos);
	DisplayList(linkedList2);
	cout << "List size = " << linkedList2.size() << endl;
	EraseElement(&linkedList2, 2);
    cout << "After erase element by index 2:" << endl;
	DisplayList(linkedList2);
    cout << "List size = " << linkedList2.size() << endl;

    /*
        Sorting and Reversing elements in a list.
    */
    list<int> randomList {-1, 20, 100, -14, 34, 0, 67, -7};
    cout << "Original random list." << endl;
    DisplayList(randomList);

    randomList.sort();
    cout << "After ascent sorting." << endl;
    DisplayList(randomList);

    randomList.reverse();
    cout << "After reversing." << endl;
    DisplayList(randomList);

    /*
        Forward list is a kind of list which allows only one direction initeration.
    */
    forward_list<int> forwardList {1, 2, 3};
    forwardList.push_front(4);
    forwardList.push_front(5);
    forwardList.push_front(6);
    DisplayList(forwardList);
    forwardList.sort();
    DisplayList(forwardList);
	return 0;
}