#include <iostream>
#include <map>
#include <unordered_map>
#include <string>
using namespace std;

template <typename T>
void DisplayMap(const T& container)
{
	cout << "Content of Map" << endl;
	for (auto i = container.cbegin(); i != container.cend(); ++ i)
	{
		cout << i->first << " -> " << i->second << endl;
	}
}

template <typename T1, typename T2>
void DisplayUnorderedMap(unordered_map<T1, T2>& container)
{
	for (auto e = container.cbegin(); e != container.cend(); ++ e)
	{
		cout << e->first << " -> " << e->second << endl;
	}
	cout << "Size: " << container.size() << endl;
	cout << "Bucket count: " << container.bucket_count() << endl;
	cout << "Current load factor: " << container.load_factor() << endl;
	cout << "Max load factor: " << container.max_load_factor() << endl;
}

template <typename keyType>
struct ReverseSort
{
	bool operator () (const keyType& key1, const keyType& key2) const
	{
		return (key1 > key2);
	}
};

int main()
{
	/*
	    Create Maps.
	    Same as Sets, Multimap allows duplicate elements.
	*/
	map<int, string> userMap;
	multimap<int, string> deviceMap;

	/*
	    Inserting pair.
	*/
	userMap.insert(make_pair(0, "root"));
	userMap.insert(make_pair(1, "system"));
	userMap.insert(make_pair(2, "service"));
	userMap.insert(make_pair(3, "nginx"));
	userMap.insert(make_pair(3, "network"));
	DisplayMap(userMap);

	deviceMap.insert(pair<int, string> (100, "Dell XPS 13"));
	deviceMap.insert(pair<int, string> (100, "Dell XPS 15"));
	deviceMap.insert(pair<int, string> (100, "Dell XPS 17"));
	deviceMap.insert(pair<int, string> (200, "ROG Zephyrus G14"));
	deviceMap.insert(pair<int, string> (200, "ROG Zephyrus G15"));
	deviceMap.insert(pair<int, string> (300, "HP Envy 14"));
	DisplayMap(deviceMap);

    // Implement user-define sort predicate.
	map<int, string, ReverseSort<int>> copyOfUserMap(userMap.cbegin(), userMap.cend());
	multimap<string, string> skillMap;
	skillMap.insert(make_pair("Java", "Hakurei"));
	skillMap.insert(make_pair("Java", "Peter"));
	skillMap.insert(make_pair("Java", "Andrew"));
	skillMap.insert(make_pair("C++", "Hakurei"));
	skillMap.insert(make_pair("C++", "Peter"));
	skillMap.insert(make_pair("Python", "Hakurei"));
	skillMap.insert(make_pair("Python", "Mike"));
	skillMap.insert(make_pair("Python", "Kevin"));
	skillMap.insert(make_pair("Python", "Lisa"));
	skillMap.insert(make_pair("R", "Hakurei"));
	skillMap.insert(make_pair("R", "Mike"));

	multimap<string, string, ReverseSort<string>> copyOfSkillMap(skillMap.cbegin(), skillMap.cend());
	DisplayMap(copyOfUserMap);
	DisplayMap(skillMap);
	DisplayMap(copyOfSkillMap);

	/*
	    Find elements by a given key.
	*/
	// Find element in Map.
	auto target = userMap.find(0);
	if (target != userMap.end())
	{
		cout << "Found target: " << target->first << " -> " << target->second << endl;
	}
	else
	{
		cout << "Not found." << endl;
	}

	target = userMap.find(10);
	if (target != userMap.end())
	{
		cout << "Found target: " << target->first << " -> " << target->second << endl;
	}
	else
	{
		cout << "Not found." << endl;
	}
    
    // Find element in MultuMap.
	auto target1 = skillMap.find("Java");
	if (target1 != skillMap.end())
	{
		size_t countOfElements = skillMap.count("Java"); // Get target quantity.
		for (size_t i = 0; i < countOfElements; ++ i)
		{
			cout << "Found target: " << target1->first << " -> " << target1->second << endl;
			++ target1;
		}
	}
	else
	{
		cout << "Not found." << endl;
	}

	target1 = skillMap.find("C");
	if (target1 != skillMap.end())
	{
		size_t countOfElements = skillMap.count("C"); // Get target quantity.
		for (size_t i = 0; i < countOfElements; ++ i)
		{
			cout << "Found target: " << target1->first << " -> " << target1->second << endl;
			++ target1;
		}
	}
	else
	{
		cout << "Not found." << endl;
	}

	/*
	    Erasing elements.
	*/
	cout << "Original Map." << endl;
	DisplayMap(userMap);
	cout << "After erasing of an element by a given key." << endl;
	userMap.erase(0);
	DisplayMap(userMap);

	cout << "After erasing of an element by a given iterator." << endl;
	auto eraseTarget = userMap.find(3);
	if (eraseTarget != userMap.end())
	{
		userMap.erase(eraseTarget);
	}
	DisplayMap(userMap);

	cout << "Original Map." << endl;
	DisplayMap(deviceMap);
	cout << "After erasing by lower_bound(100) and upper_bound(200)." << endl;
	deviceMap.erase(deviceMap.lower_bound(100), deviceMap.upper_bound(200));
	DisplayMap(deviceMap);

	/*
	    Unordered map.
	    Same as unordered_sat, unordered_map is Hash-table based key-value container.
	*/
    unordered_map<int, string> laptopMap;
    laptopMap.insert(pair<int, string> (100, "Dell XPS 13"));
	laptopMap.insert(pair<int, string> (120, "Dell XPS 15"));
	laptopMap.insert(pair<int, string> (-500, "Dell XPS 17"));
	laptopMap.insert(pair<int, string> (-200, "ROG Zephyrus G14"));
	laptopMap.insert(pair<int, string> (10, "ROG Zephyrus G15"));
	laptopMap.insert(pair<int, string> (78, "HP Envy 14"));
	cout << "unordered_map sample." << endl;
	DisplayUnorderedMap(laptopMap);
	return 0;
}