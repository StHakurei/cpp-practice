#include <string>
#include <list>
#include <iostream>
using namespace std;

template <typename T>
void showContacts(const T& container)
{
	for (auto i = container.cbegin(); i != container.cend(); ++i)
	{
		cout << *i << endl;
	}
}

struct PlayerInfo
{
	string playerName;
	string phoneNumber;
	string regionCode;
	string displayAs;

	PlayerInfo(const string& name, const string& mobile, const string& region)
	{
		playerName = name;
		phoneNumber = mobile;
		regionCode = region;
		displayAs = (playerName + ": +" + regionCode + " " + phoneNumber);
	}

	bool operator == (const PlayerInfo& palyer) const
	{
		return (this->playerName == palyer.playerName);
	}

	bool operator < (const PlayerInfo& palyer) const
	{
		return (this->playerName < palyer.playerName);
	}

	operator const char*() const
	{
		return displayAs.c_str();
	}
};

bool SortOnPhoneNumber(const PlayerInfo& player0, const PlayerInfo& player1)
{
	return (player0.phoneNumber < player1.phoneNumber);
}

bool SortOnRegion(const PlayerInfo& player0, const PlayerInfo& player1)
{
	return (player0.regionCode < player1.regionCode);
}

int main()
{
	list<PlayerInfo> playerContacts;
	playerContacts.push_back(PlayerInfo("Langston Hakurei", "23488768276", "1"));
	playerContacts.push_back(PlayerInfo("Ishtar", "2762263028", "56"));
	playerContacts.push_back(PlayerInfo("Bill", "10288808276", "2"));
	playerContacts.push_back(PlayerInfo("Andrew", "12388768000", "44"));
	playerContacts.push_back(PlayerInfo("Ereshkigal", "12200762376", "23"));
	playerContacts.push_back(PlayerInfo("Peter", "9086657786", "106"));
	cout << "Original contacts:" << endl;
	showContacts(playerContacts);
	cout << endl;

	playerContacts.sort();
	cout << "Sorting in alphabetical order via operator <:" << endl;
	showContacts(playerContacts);
	cout << endl;

	playerContacts.sort(SortOnPhoneNumber);
	cout << "Sorting in order of phone numbers via predicate:" << endl;
	showContacts(playerContacts);
	cout << endl;

	playerContacts.sort(SortOnRegion);
	cout << "Sorting in order of region code via predicate:" << endl;
	showContacts(playerContacts);
	cout << endl;

	playerContacts.remove(PlayerInfo("Ishtar", "", ""));
	cout << "Remove Ishtar from the contacts:" << endl;
	showContacts(playerContacts);
	cout << endl;
	return 0;
}