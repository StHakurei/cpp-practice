#include <set>
#include <unordered_set>
#include <iostream>
#include <string>
using namespace std;

template <typename T>
void DisplaySet(const T& container)
{
    for (auto i = container.cbegin(); i != container.cend(); ++ i)
    {
        cout << *i << " ";
    }
    cout << endl;
}

/*
    By default, both set and multiset will sort element on insertion, they use
    a predicate std:less when you don't supply a sort criteria, this ensures the
    set contains elements are sorted in ascending order.

    But you can create a binary sort predicate by defining a class with operator '()'
    that takes two elements and returns TRUE depending on your criteria.
*/
template <typename T>
struct SortDescending
{
    bool operator () (const T& left, const T& right) const
    {
        return (left > right);
    }
};

int main()
{
    /*
        Create sets.
    */

    set<int> codeSet;
    multiset<string> nameSet;
    
    // Create a set by given user-defined sort predicate.
    set<int, SortDescending<int>> descentCodeSet;
    multiset<string, SortDescending<string>> descentNameSet;

    // Create a set from other container.
    set<int> copyOfCodeSet(codeSet);
    multiset<string> copyOfNameSet(nameSet);

    /*
        Inserting elements.
    */
    set<int> defaultSet {-10, 0, 10};
    cout << "Original set: ";
    DisplaySet(defaultSet);
    defaultSet.insert(-10); // Deplicate.
    cout << "After duplicate: ";
    DisplaySet(defaultSet);
    cout << "Number of element '-10' in the set: " << defaultSet.count(-10) << endl;

    // Multiset allows duplicate elements.
    multiset<int> defaultMSet {-10, 0, 10};
    cout << "Original multiset: ";
    DisplaySet(defaultMSet);
    defaultMSet.insert(-10); // Duplicate.
    cout << "After duplicate: ";
    DisplaySet(defaultMSet);
    cout << "Number of element '-10' in the multiset: " << defaultMSet.count(-10) << endl;


    /*
        Seraching elements.
    */

    // Prepare some elements for searching.
    codeSet = {100, -50, 23, -8, 99, 104, 76, -32};
    nameSet = {"Hakurei", "Dell", "Sakura", "Joma", "Peter", "Ereshkigal"};

    // Saerch element.
    auto findCode = codeSet.find(99);
    if (findCode != codeSet.end())
    {
        cout << "Found target: " << *findCode << endl;
    }
    else
    {
        cout << "Not found." << endl;
    }

    findCode = codeSet.find(59);
    if (findCode != codeSet.end())
    {
        cout << "Found target: " << *findCode << endl;
    }
    else
    {
        cout << "Not found." << endl;
    }

    auto findName = nameSet.find("Hakurei");
    if (findName != nameSet.end())
    {
        cout << "Found target: " << *findName << endl;
    }
    else
    {
        cout << "Not found." << endl;
    }

    findName = nameSet.find("Ishtar");
    if (findName != nameSet.end())
    {
        cout << "Found target: " << *findName << endl;
    }
    else
    {
        cout << "Not found." << endl;
    }

    /*
        Erasing elements.
    */
    set<int> sampleSet(codeSet.cbegin(), codeSet.cend());
    cout << "Original set: ";
    DisplaySet(sampleSet);
    cout << "Set contains elements: " << sampleSet.size() << endl;
    sampleSet.erase(104);
    cout << "After erasing: ";
    DisplaySet(sampleSet);
    cout << "Set contains elements: " << sampleSet.size() << endl;

    // Erase element by value in multiset will erase all elements which are matched.
    multiset<int> multiSet{10, 20, 30, 100, 100, 101, 201};
    cout << "Original multiset: ";
    DisplaySet(multiSet);
    cout << "Multiset contains elements: " << multiSet.size() << endl;
    multiSet.erase(100);
    cout << "After erasing: ";
    DisplaySet(multiSet);
    cout << "Multiset contains elements: " << multiSet.size() << endl;

    /*
        Set and Multiset have advantages in search because they are sorted. However,
        to provide this advantage, the container needs to sort element at insertion time.

        Set and Multiset are maintaining an internal binary tree structure, but C++ 11 provides
        another approach with Hash Set Implementation.
    */
    unordered_set<int> hashSet{100, 102, 302, -123, -12};
    unordered_multiset<int> hashMultiset{100, 100, -321, -19, 45};
    cout << "Original unordered set and multiset:" << endl;
    DisplaySet(hashSet);
    DisplaySet(hashMultiset);

    hashSet.insert(-100);
    hashSet.insert(255);
    hashMultiset.insert(-100);
    hashMultiset.insert(-100);
    cout << "After insertion:" << endl;
    DisplaySet(hashSet);
    DisplaySet(hashMultiset);

    cout << "Bucket count: " << hashSet.bucket_count() << endl;
    cout << "Max load factor: " << hashSet.max_load_factor() << endl;
    cout << "Load factor: " << hashSet.load_factor() << endl;
    return 0;
}