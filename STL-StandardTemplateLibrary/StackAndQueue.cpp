#include <iostream>
#include <stack>
#include <queue>
#include <functional>
using namespace std;

void StackSample()
{
    /*
        Stack is a FILO data structure.
    */
    cout << "** STL Container - Stack **" << endl;
    stack<int> sample;
    cout << "Pushing {1, 2, 3, 4, 5} into the Stack." << endl;
    sample.push(1);
    sample.push(2);
    sample.push(3);
    sample.push(4);
    sample.push(5);

    cout << "Stack size: " << sample.size() << endl;
    while (sample.size() != 0)
    {
        cout << "Poping element: " << sample.top() << endl; // Peek the top element.
        sample.pop(); // Take away the top element.
    }

    if (sample.empty())
    {
        cout << "Stack is empty now!" << endl;
    }
}

void QueueSample()
{
    /*
        Queue is a FIFO data structure.
    */
    cout << "** STL Container - Queue **" << endl;
    queue<int> sample;
    cout << "Pushing {1, 2, 3, 4, 5} into the Stack." << endl;
    sample.push(1);
    sample.push(2);
    sample.push(3);
    sample.push(4);
    sample.push(5);

    cout << "Stack size: " << sample.size() << endl;
    cout << "The front element is: " << sample.front() << endl;
    cout << "The back element is: " << sample.back() << endl;
    while (sample.size() != 0)
    {
        cout << "Poping element: " << sample.front() << endl; // Peek the front element.
        sample.pop(); // Take away the front element.
    }

    if (sample.empty())
    {
        cout << "Stack is empty now!" << endl;
    }
}

void PriorityQueue()
{
    /*
        Priority Queue is a FIFO data structure and keep the hightest value
        (Or deemed as highest value by a binary predicate) in the front of the queue.
    */
    cout << "** STL Container - Priority Queue **" << endl;
    priority_queue<int> sample;
    sample.push(10);
    sample.push(-1);
    sample.push(9);
    sample.push(12);
    sample.push(-5);

    cout << "Stack size: " << sample.size() << endl;
    cout << "The highest value in the front:" << endl;
    while (sample.size() != 0)
    {
        cout << "Poping element: " << sample.top() << endl; // Peek the front element.
        sample.pop(); // Take away the front element.
    }

    if (sample.empty())
    {
        cout << "Stack is empty now!" << endl;
    }

    priority_queue<int, vector<int>, greater<int>> sample1;
    sample1.push(10);
    sample1.push(-1);
    sample1.push(9);
    sample1.push(12);
    sample1.push(-5);
    cout << "Stack size: " << sample1.size() << endl;
    cout << "The lowest value in the front:" << endl;
    while (sample1.size() != 0)
    {
        cout << "Poping element: " << sample1.top() << endl; // Peek the front element.
        sample1.pop(); // Take away the front element.
    }

    if (sample1.empty())
    {
        cout << "Stack is empty now!" << endl;
    }   
}

int main()
{
    StackSample();
    QueueSample();
    PriorityQueue();
    return 0;
}