#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main()
{
    // Declare string.
    string sentence("In thy light, I can see the light.");

    // Display characters of the string.
    /*
        size_t is an Alias of one of the fundamental unsigned integer types.
        It is a type able to represent the size of any object in bytes: size_t is 
        the type returned by the sizeof operator and is widely used in the standard library to represent sizes and counts.
    */
    cout << "============ Access characters via array syntax ============" << endl;
    for (size_t charIndex = 0; charIndex < sentence.length(); ++charIndex)
    {
        cout << "Character index [" << charIndex << "] = " << sentence[charIndex] << endl;
    }

    // Display characters of the string by using iterator.
    int offset = 0;
    string::const_iterator anker;
    cout << "============ Access characters via iterator ============" << endl;
    for (auto anker = sentence.begin(); anker != sentence.end(); anker++ )
    {
        cout << "Character index [" << offset++ << "] = " << *anker << endl;
    }

    string part0("Artoria");
    string part1(" Pendragon");
    // Concatenating strings by using operator.
    cout << "Saber's name: " << part0 + part1 << endl;
    cout << "Saber's name: " << part0.append(part1) << endl;

    // Find sub-string or char in a string.
    size_t position = sentence.find("light", 0);
    if (position != string::npos)
    {
        cout << "Found content at the position " << position << endl;
    }
    else
    {
        cout << "No return position." << endl;
    }

    string target ("C++ is a general-purpose programming language created by Bjarne Stroustrup as an extension of the C programming language, or 'C with Classes'.");
    // Delete content with given postion and count(length).
    cout << "Before: " << target << endl;
    target.erase(120, 20);
    cout << "After: " << target << endl;

    // Delete content by using iterator.
    string::iterator charIndex = find(target.begin(), target.end(), 'B');
    // If found character, delete it by 'erase'.
    if (charIndex != target.end())
    {
        target.erase(charIndex);
    }
    cout << "And after: " << target << endl;

    // Erase a range between begin() and end().
    target.erase(target.begin(), target.end());
    if (target.length() == 0)
    {
        cout << "And string is empty now." << endl;
    }

    // String reversal.
    cout << "Before reversal: " << part0 << endl;
    reverse(part0.begin(), part0.end());
    cout << "After reversal: " << part0 << endl;

    // String case conversion.
    transform(part0.begin(), part0.end(), part0.begin(), ::toupper);
    cout << "After conversion: " << part0 << endl;
    
    // C++14 operator "" in std::string.
    string cpp14String("\"United \0Status of America\""s);
    cout << "C++ 14 string initialization sample." << endl;
    cout << cpp14String << " - Length: " << cpp14String.length() << endl;

    string cppString("\"United \0Status of America\"");
    cout << "Traditional string initialization sample." << endl;
    cout << cppString << " - Length: " << cppString.length() << endl;
    return 0;
}