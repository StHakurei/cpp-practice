#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
using namespace std;

void CreateFile()
{
    cout << "** Creating & Writing Files **" << endl;
    ofstream myFile;
    myFile.open("Hello.txt", ios_base::out);

    if (myFile.is_open())
    {
        cout << "Open file successfully." << endl;

        myFile << "First line." << endl;
        myFile << "Second line." << endl;

        cout << "Writing finished." << endl;
        myFile.close();
    }
}

void ReadFile()
{
    cout << "** Reading Files **" << endl;
    ifstream myFile;
    myFile.open("story.txt", ios_base::in);

    if (myFile.is_open())
    {
        cout << "Open file successfully." << endl;
        string buffer;
        
        cout << "------ Content Start ------" << endl;
        while (myFile.good())
        {
            getline(myFile, buffer);
            cout << buffer << endl;
        }
        cout << "------  Content End  ------" << endl;

        cout << "Reading file finished." << endl;
        myFile.close();
    }
    else
    {
        cout << "Failed to open file." << endl;
    }
}

void BinaryFile()
{
    cout << "** Reading & Writing Binary Files **" << endl;

    struct Data
    {
        char name[30];
        int age;
        char DOB[20];
        Data() {};
        Data(const char* inName, const int inAge, const char* inDOB): age(inAge)
        {
            strcpy(name, inName);
            strcpy(DOB, inDOB);
        }
    };

    Data newData("Langston.Hakurei", 980, "Nov 2018");

    ofstream biFileOut("data.bin", ios_base::out | ios_base::binary);
    if (biFileOut.is_open())
    {
        cout << "Writing object to a binary file." << endl;
        biFileOut.write(reinterpret_cast<const char*>(&newData), sizeof(newData));
        biFileOut.close();
    }

    ifstream biFileIn("data.bin", ios_base::in | ios_base::binary);
    if (biFileIn.is_open())
    {
        cout << "Reading binary file." << endl;
        Data readObject;
        biFileIn.read((char*)&readObject, sizeof(readObject));

        cout << "------ Content of Object ------" << endl;
        cout << "Name: " << readObject.name << endl;
        cout << "Age: " << readObject.age << endl;
        cout << "Birth of Data: " << readObject.DOB << endl;
        cout << "------  End of Content   ------" << endl;

        cout << "Done, close now." << endl;
        biFileIn.close();
    }
}

void Conversions()
{
    cout << "** String Conversions **" << endl;
    cout << "Enter an interger:";
    int input;
    cin >> input;
    
    // Convert int to string.
    stringstream toString;
    toString << input;
    string inputStr;
    toString >> inputStr;

    cout << "Integer input: " << input << endl;
    cout << "String input: " << inputStr << endl;

    // Convert string to int.
    stringstream toInt;
    toInt << inputStr;
    int copy = 0;
    toInt >> copy;
    cout << "Integer gained from String: " << copy << endl;
}

int main()
{
    CreateFile();
    ReadFile();
    BinaryFile();
    Conversions();
    return 0;
}