#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

void ChangingNumberFormats()
{
    cout << "** Changing Display Number Formats **" << endl;
    cout << "Enter a number:";
    int input = 0;
    cin >> input;

    cout << "Integer in octal: " << oct << input << endl;
    cout << "Integer in hexadecimal: " << hex << input << endl;

    cout << "Integer in hex using base notation: ";
    cout << setiosflags(ios_base::hex | ios_base::showbase | ios_base::uppercase);
    cout << input << endl;

    cout << "Integer after resetting I/O flags: ";
    cout << resetiosflags(ios_base::hex | ios_base::showbase | ios_base::uppercase);
    cout << input << endl;
}

void FixedPoint()
{
    cout << "** Fixed-Point and Scientific Notation **" << endl;
    const double Pi = (double)22.0 / 7;
    cout << "Pi = " << Pi << endl;
    cout << "Setting precision to 7:" << endl;
    cout << setprecision(7);
    cout << "Pi = " << Pi << endl;
    cout << fixed << "Fixed Pi = " << Pi << endl;
    cout << scientific << "Scientific Pi = " << Pi << endl;
}

void AligningText()
{
    cout << "** Aligning Text **" << endl;
    cout << setw(10);
    cout << "Name";
    cout << ":" << "Langston.Hakurei" << endl;

    cout << setw(10);
    cout << "Mobile";
    cout << ":" << "+1 123-456-7890" << endl;

    cout << setw(10);
    cout << "Email";
    cout << ":" << "langston@hakurei.moe" << endl;
}

int main()
{
    ChangingNumberFormats();
    FixedPoint();
    AligningText();
    return 0;
}