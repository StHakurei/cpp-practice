#include <iostream>
#include <string.h>
using namespace std;

int CPPString()
{
    /* C++ String. */
    #include <string>
    string words ("This is C++ string.");
    cout << words << endl;

    /* Read user input into C++ string. */
    cout << "Enter a line of words: " << endl;
    string oneString;
    getline(cin, oneString);

    cout << "Maybe another line of words: " << endl;
    string another;
    getline(cin, another);

    string result = oneString + another;
    cout << "Your words: " << result << endl;
    return 0;
}

int main()
{
    /* C-Style string. */
    char city[] = {'R', 'o', 'm', 'a'};
    cout << city << endl;
    cout << "String lenght: " << strlen(city) << endl;
    return CPPString();
}