#include <iostream>

int main()
{
    /* Unsigned variables support larger positive number.

    short = -32,768 to 32,768
    unsigned short = 0 - 4,294,967,295
    */
    short int shortCode = -100;
    unsigned short int positiveShortCode = 100;

    std::cout << shortCode << ":" << positiveShortCode << std::endl;

    /* The number will always be positive.

    Then you will get 65436, it's not what you've declared.
    So don't do it.
    */
    unsigned short justTry = -100;
    std::cout << "BAD SAMPLE:" << justTry << std::endl;

    /* Re-define variables. */
    int code = 123456;
    std::cout << "Password:" << code << std::endl;
    code = 654321;
    std::cout << "Reversed: " << code << std::endl;

    /* Constant variables. */
    const unsigned long secret = 1234567890;
    /* You will get below error if call 'secret = 0'

    Variables.cpp: In function ‘int main()’:
    Variables.cpp:29:12: error: assignment of read-only variable ‘secret’
    29 |     secret = 0;
       |     ~~~~~~~^~~
    */

    /* Automatic detect variable type, but you have to initialize the value. */
    auto something = true;

    /* Declared and define value laterly. */
    char a, b, c;
    a = 'A';
    b = 'b';
    c = 'c';
    std::cout << "a: " << a << std::endl;
    std::cout << "b: " << b << std::endl;
    std::cout << "c: " << c << std::endl;

    /* Avoid narrowing conversion, it's stupid, just don't do it.
    int someNum = 500000;
    short another = someNum;
    */

    return 0;
}